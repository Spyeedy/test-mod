package spyeedy.testmod.ability;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataInteger;
import lucraft.mods.lucraftcore.superpowers.render.RenderSuperpowerLayerEvent;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.suitset.TMSuitSet;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AbilitySuitUp extends AbilityToggle {

	public static int MAX_TIMER = 40;
	public static AbilityData<Integer> ANIM_TIMER = new AbilityDataInteger("anim_timer");

	public AbilitySuitUp(EntityLivingBase entity) {
		super(entity);
		this.dataManager.register(ANIM_TIMER, MAX_TIMER);
	}

	@Override
	public boolean action() {
		boolean b = super.action();
		if (isEnabled()) {
			for (EntityEquipmentSlot slots : EntityEquipmentSlot.values()) {
				if (slots.getSlotType() == EntityEquipmentSlot.Type.ARMOR) {
					EntityPlayer player = (EntityPlayer) getEntity();
					player.dropItem(player.getItemStackFromSlot(slots), false);
					player.setItemStackToSlot(slots, ItemStack.EMPTY);
				}
			}
		}
		return b;
	}

	@Override
	public void updateTick() {
		if (isEnabled()) {
			int timer = getDataManager().get(ANIM_TIMER);

			boolean saveTimer = false;
			boolean disableAb = false;
			if (timer < MAX_TIMER) {
				timer++;
				saveTimer = true;
			} else {
				timer = 0;
				saveTimer = true;

				EntityPlayer player = (EntityPlayer) getEntity();
				/*if (TMSuitSet.RING_SUIT_UP.getHelmet() != null)
					player.setItemStackToSlot(EntityEquipmentSlot.HEAD, new ItemStack(TMSuitSet.RING_SUIT_UP.getHelmet()));
				if (TMSuitSet.RING_SUIT_UP.getChestplate() != null)
					player.setItemStackToSlot(EntityEquipmentSlot.CHEST, new ItemStack(TMSuitSet.RING_SUIT_UP.getChestplate()));
				if (TMSuitSet.RING_SUIT_UP.getLegs() != null)
					player.setItemStackToSlot(EntityEquipmentSlot.LEGS, new ItemStack(TMSuitSet.RING_SUIT_UP.getLegs()));
				if (TMSuitSet.RING_SUIT_UP.getBoots() != null)
					player.setItemStackToSlot(EntityEquipmentSlot.FEET, new ItemStack(TMSuitSet.RING_SUIT_UP.getBoots()));*/

				disableAb = true;
			}

			if (saveTimer) {
				getDataManager().set(ANIM_TIMER, timer);
			}

			if (disableAb) {
				action();
			}
		}
	}

	public static Map<String, Object> SUIT_TEXTURES = new HashMap<>();

	@SideOnly(Side.CLIENT)
	public void bindArmorTexture(SuitSet suitSet, boolean glow, EntityEquipmentSlot slot, boolean smallArms) {
		int timer = getDataManager().get(ANIM_TIMER);
		String key = suitSet.getRegistryName().toString() + (glow ? "_glow" : "_normal") + (smallArms ? "_smallarms" : "") + "_" + timer;

		ResourceLocation normalTex = new ResourceLocation(suitSet.getModId(), "textures/models/armor/" + suitSet.getRegistryName().getPath() + "/normal" + (smallArms ? "_smallarms" : "") + ".png");
		ResourceLocation glowTex = new ResourceLocation(suitSet.getModId(), "textures/models/armor/" + suitSet.getRegistryName().getPath() + "/glow" + (smallArms ? "_smallarms" : "") + ".png");

		if (timer == MAX_TIMER) {
			return;
		}

		System.out.println(key);

		try {
			DynamicTexture t = null;
			if (SUIT_TEXTURES.containsKey(key))
				t = (DynamicTexture) SUIT_TEXTURES.get(key);
			else {
				BufferedImage image = TextureUtil.readBufferedImage(Minecraft.getMinecraft().getResourceManager().getResource(glow ? glowTex : normalTex).getInputStream());

					// Helmet
					float helmetProgress = (float) MathHelper.clamp(timer - 10, 0, 10) / 10;
					animY(image, 0, 17, 64, 8, helmetProgress, helmetProgress != 1F, suitSet);
					animY(image, 8, 0, 16, 8, helmetProgress == 1F ? 1F : 0F, false, null);
					animY(image, 40, 0, 48, 8, helmetProgress == 1F ? 1F : 0F, false, null);
					animY(image, 16, 0, 24, 8, helmetProgress > 0F ? 1F : 0F, false, null);
					animY(image, 48, 0, 56, 8, helmetProgress > 0F ? 1F : 0F, false, null);

					// Arms
					float mainHandProgress = (float) MathHelper.clamp(timer, 0, 10) / 10;
					animY(image, 40, 32, 56, 20, mainHandProgress, suitSet); // right arm - sides
					animY(image, 40, 36, 56, 48, mainHandProgress, suitSet); // overlay right arm - sides
					animY(image, 44, 16, smallArms ? 47 : 48, 20, mainHandProgress == 1F ? 1F : 0F, false, null); // right arm - top
					animY(image, 44, 32, smallArms ? 47 : 48, 36, mainHandProgress == 1F ? 1F : 0F, false, null); // overlay right arm - top
					animY(image, smallArms ? 47 : 48, 16, 52, 20, mainHandProgress > 0F ? 1F : 0F, false, null); // right arm - bottom
					animY(image, smallArms ? 47 : 48, 32, 52, 36, mainHandProgress > 0F ? 1F : 0F, false, null); // overlay right arm - bottom

					// Chest
					float chestProgress = (float) MathHelper.clamp(timer - 10, 0, 10) / 10;
					animY(image, 16, 20, 40, 32, chestProgress, suitSet);
					animY(image, 16, 36, 40, 48, chestProgress, suitSet);
					animY(image, 28, 16, 36, 20, chestProgress == 1F ? 1F : 0F, false, null);
					animY(image, 28, 32, 36, 36, chestProgress == 1F ? 1F : 0F, false, null);
					animY(image, 20, 16, 28, 20, chestProgress > 0F ? 1F : 0F, false, null);
					animY(image, 20, 32, 28, 36, chestProgress > 0F ? 1F : 0F, false, null);

					animY(image, 32, 52, 48, 64, chestProgress, suitSet); // left arm - sides
					animY(image, 48, 52, 64, 64, chestProgress, suitSet); // overlay left arm - sides
					animY(image, 36, 48, smallArms ? 39 : 40, 52, chestProgress > 0F ? 1F : 0F, false, null); // left arm - top
					animY(image, 52, 48, smallArms ? 55 : 56, 52, chestProgress > 0F ? 1F : 0F, false, null); // overlay left arm - top
					animY(image, smallArms ? 39 : 40, 48, 44, 52, chestProgress == 1F ? 1F : 0F, false, null); // left arm - bottom
					animY(image, smallArms ? 55 : 56, 48, 60, 52, chestProgress == 1F ? 1F : 0F, false, null); // overlay left arm - bottom

					// Legs
					float legsProgress = (float) MathHelper.clamp(timer - 20, 0, 10) / 10;
					animY(image, 0, 20, 16, 32, legsProgress, legsProgress != 0F, suitSet);
					animY(image, 0, 36, 16, 48, legsProgress, legsProgress != 0F, suitSet);
					animY(image, 0, 52, 16, 64, legsProgress, legsProgress != 0F, suitSet);
					animY(image, 16, 52, 32, 64, legsProgress, legsProgress != 0F, suitSet);
					animY(image, 4, 16, 8, 20, legsProgress > 0F ? 1F : 0F, false, null);
					animY(image, 4, 32, 8, 36, legsProgress > 0F ? 1F : 0F, false, null);
					animY(image, 8, 16, 12, 20, legsProgress == 1F ? 1F : 0F, false, null);
					animY(image, 8, 32, 12, 36, legsProgress == 1F ? 1F : 0F, false, null);

					animY(image, 20, 48, 24, 52, legsProgress > 0F ? 1F : 0F, false, null);
					animY(image, 4, 48, 8, 52, legsProgress > 0F ? 1F : 0F, false, null);
					animY(image, 24, 48, 28, 52, legsProgress == 1F ? 1F : 0F, false, null);
					animY(image, 8, 48, 16, 52, legsProgress == 1F ? 1F : 0F, false, null);

				/*for (int x = 0; x < 64; x++) {
					for (int y = 0; y < 64; y++) {
						int rgb = overlay.getRGB(x, y);
						if (rgb != 0) {
							image.setRGB(x, y, rgb);
						}
					}
				}*/

				t = new DynamicTexture(image);
				SUIT_TEXTURES.put(key, t);
			}
			GlStateManager.bindTexture(t.getGlTextureId());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SideOnly(Side.CLIENT)
	public void animY(BufferedImage image, int startX, int startY, int endX, int endY, float progress, SuitSet suitSet) {
		animY(image, startX, startY, endX, endY, progress, true, suitSet);
	}

	@SideOnly(Side.CLIENT)
	public void animY(BufferedImage image, int startX, int startY, int endX, int endY, float progress, boolean trans, SuitSet suitSet) {
		for (int x = Math.min(startX, endX); x < Math.max(startX, endX); x++) {
			for (int y = Math.min(startY, endY); y < Math.max(startY, endY); y+=2) {
				System.out.println("progress:" + progress + " y:" + y);
				int i = image.getRGB(x, y);
				if (i != 0 && Integer.toHexString(i).length() > 2) {
					boolean vis = startY < endY ? (y < startY + progress * (endY - startY)) : (y >= startY + progress * (endY - startY)); // decides visibility of pixels
					String rgb = Integer.toHexString(i).substring(2);
					String newRgb = (vis ? "ff" : "00") + rgb; // AARRGGBB, AA sets transparency of pixel
					image.setRGB(x, y, (int) Long.parseLong(newRgb, 16));
				}
				int i2 = image.getRGB(x, y+1);
				if (i2 != 0 && Integer.toHexString(i2).length() > 2) {
					boolean vis2 = startY < endY ? (y + 1 < startY + progress * (endY - startY)) : (y + 1 >= startY + progress * (endY - startY)); // decides visibility of pixels
					String rgb2 = Integer.toHexString(i2).substring(2);
					String newRgb2 = (vis2 ? "ff" : "00") + rgb2; // AARRGGBB, AA sets transparency of pixel
					image.setRGB(x, y+1, (int) Long.parseLong(newRgb2, 16));
				}

				/*if (trans && y == (int) (startY + progress * (endY - startY))) {
					image.setRGB(x, y, (int) Long.parseLong("ff" + "0000ff", 16));
				}*/
			}
		}
	}


	@Mod.EventBusSubscriber(modid = TestMod.MODID, value = Side.CLIENT)
	public static class Renderer {

		@SubscribeEvent
		public static void onRenderSuperpowerLayer(RenderSuperpowerLayerEvent event) {
			for (Ability ab : Ability.getAbilities(event.getPlayer())) {
				if (ab instanceof AbilitySuitUp) {
					AbilitySuitUp ability = (AbilitySuitUp) ab;

					boolean startAnim = ability.isEnabled();
					int timer = ability.getDataManager().get(AbilitySuitUp.ANIM_TIMER);
					boolean smallArms = PlayerHelper.hasSmallArms(event.getPlayer());

					if (startAnim && timer < AbilitySuitUp.MAX_TIMER) {
//						doStuffs(ability, event, smallArms, EntityEquipmentSlot.HEAD);
						doStuffs(ability, event, smallArms, EntityEquipmentSlot.CHEST);
//						doStuffs(ability, event, smallArms, EntityEquipmentSlot.LEGS);
//						doStuffs(ability, event, smallArms, EntityEquipmentSlot.FEET);
					}
				}
			}
		}

		private static void doStuffs(AbilitySuitUp ability, RenderSuperpowerLayerEvent event, boolean smallArms, EntityEquipmentSlot slot) {
			ability.bindArmorTexture(TMSuitSet.RING_SUIT_UP, false, slot, smallArms);
			GlStateManager.enableBlend();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			event.getRenderPlayer().getMainModel().render(event.getPlayer(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getAgeInTicks(), event.getNetHeadYaw(), event.getHeadPitch(), event.getScale());

			if (TMSuitSet.RING_SUIT_UP.hasGlowyThings(event.getPlayer(), slot)) {
				ability.bindArmorTexture(TMSuitSet.RING_SUIT_UP, true, slot, smallArms);
				GlStateManager.pushMatrix();
				GlStateManager.color(1, 1, 1, TMSuitSet.RING_SUIT_UP.getGlowOpacity(null, null, null));
				GlStateManager.disableLighting();
				float lastBrightnessX = OpenGlHelper.lastBrightnessX;
				float lastBrightnessY = OpenGlHelper.lastBrightnessY;

				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240F, 240F);
				event.getRenderPlayer().getMainModel().render(event.getPlayer(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getAgeInTicks(), event.getNetHeadYaw(), event.getHeadPitch(), event.getScale());
				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lastBrightnessX, lastBrightnessY);
				GlStateManager.disableBlend();
				GlStateManager.enableLighting();
				GlStateManager.popMatrix();
			}
		}
	}
}