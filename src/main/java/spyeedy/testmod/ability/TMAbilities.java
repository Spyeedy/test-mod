package spyeedy.testmod.ability;

import lucraft.mods.lucraftcore.superpowers.abilities.AbilityEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.TestMod;

@Mod.EventBusSubscriber(modid = TestMod.MODID)
public class TMAbilities {

	@SubscribeEvent
	public static void registerAbilities(RegistryEvent.Register<AbilityEntry> e) {
		e.getRegistry().register(new AbilityEntry(AbilitySuitUp.class, new ResourceLocation(TestMod.MODID, "suit_up")));
	}
}