package spyeedy.testmod.addonpack.model.modeltype;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.Event;

public class RegisterModelTypeEvent extends Event {

	public void register(ResourceLocation regName, Class<? extends ModelType> clazz) {
		ModelType.MODEL_TYPE_REGISTRY.put(regName, clazz);
	}
}