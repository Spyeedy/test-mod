package spyeedy.testmod.addonpack.model.modeltype;

import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import spyeedy.testmod.addonpack.model.client.model.IJsonModel;
import spyeedy.testmod.addonpack.model.client.model.ModelJsonBiped;

public class ModelTypeBiped extends ModelType {

	@Override
	@SideOnly(Side.CLIENT)
	public Class<? extends IJsonModel> getModelClass() {
		return ModelJsonBiped.class;
	}

	@Override
	public NonNullList<String> getParts() {
		NonNullList<String> parts = NonNullList.create();

		parts.add("head");
		parts.add("body");
		parts.add("right_arm");
		parts.add("left_arm");
		parts.add("right_leg");
		parts.add("left_leg");

		return parts;
	}
}