package spyeedy.testmod.addonpack.model.modeltype;

import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.addonpack.model.client.model.IJsonModel;

import java.util.HashMap;

@Mod.EventBusSubscriber
public abstract class ModelType {

	public static HashMap<ResourceLocation, Class<? extends ModelType>> MODEL_TYPE_REGISTRY = new HashMap<>();

	@SubscribeEvent
	public static void registerModelTypes(RegisterModelTypeEvent e) {
		e.register(new ResourceLocation(TestMod.MODID, "biped"), ModelTypeBiped.class);
	}

	// -----------------------------------------------------------------------------------------

	@SideOnly(Side.CLIENT)
	public abstract Class<? extends IJsonModel> getModelClass();

	public abstract NonNullList<String> getParts();
}
