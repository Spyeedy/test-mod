package spyeedy.testmod.addonpack.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import spyeedy.testmod.addonpack.model.modeltype.ModelType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;

public class JsonModelInfo {

	@Nullable
	public ResourceLocation parent;
	public HashMap<String, ArrayList<JsonModelCube>> subpart_cubes;
	
	public String name;
	public int[] texture_size;
	public String color_texture;
	public String texture;
	public ArrayList<JsonModelCube> model_cubes;

	public void setParentModel(ResourceLocation parent) {
		this.parent = parent;
	}
	
	public void setSubPartCubes(HashMap<String, ArrayList<JsonModelCube>> subpart_cubes) {
		this.subpart_cubes = subpart_cubes;
	}
	
	public void setModelName(String name) {
		this.name = name;
	}
	
	public void setTextureSize(int[] size) {
		this.texture_size = size;
	}

	public void setColorTextureName(String texturePath) {
		this.color_texture = texturePath;
	}
	
	public void setTextureName(String texturePath) {
		this.texture = texturePath;
	}

	public void setModelCubes(ArrayList<JsonModelCube> cubes) {
		this.model_cubes = cubes;
	}

	public void deserialize(JsonObject object) throws Exception {
		ResourceLocation parent = JsonUtils.hasField(object, "parent") ? new ResourceLocation(JsonUtils.getString(object, "parent")) : null;
		if (parent != null && !ModelType.MODEL_TYPE_REGISTRY.containsKey(parent)) {
			throw new Exception("Invalid value for 'parent'");
		}
		
		String name = JsonUtils.getString(object, "name");
		int[] texture_size = new int[]{ JsonUtils.getJsonArray(object, "texture_size").get(0).getAsInt(), JsonUtils.getJsonArray(object, "texture_size").get(1).getAsInt() };

		JsonObject textureObject = JsonUtils.getJsonObject(object, "textures");
		String color_texture_name = JsonUtils.getString(textureObject, "color_texture");
		String texture_name = JsonUtils.getString(textureObject, "texture", "");
		
		ArrayList<JsonModelCube> cubes = new ArrayList<>();
		for (int i = 0; i < JsonUtils.getJsonArray(object, "model_cubes").size(); i++) {
			JsonObject cubeObject = JsonUtils.getJsonArray(object, "model_cubes").get(i).getAsJsonObject();
			
			JsonModelCube cube = new JsonModelCube();
			cube.deserialize(cubeObject);
			deserializeChildCubes(cubeObject, cube);
			
			cubes.add(cube);
		}

		HashMap<String, ArrayList<JsonModelCube>> subparts_cubes = new HashMap<>();
		if (parent != null) {
			ModelType modelType = ModelType.MODEL_TYPE_REGISTRY.get(parent).newInstance();

			if (modelType != null) {
				JsonObject subPartCubesObject = JsonUtils.getJsonObject(object, "subpart_cubes");
				for (String partName : modelType.getParts()) {
					ArrayList<JsonModelCube> subpartCubes = new ArrayList<>();

					JsonArray jsonSubpartCubesArr = JsonUtils.getJsonArray(subPartCubesObject, partName.toLowerCase());
					for (int i = 0; i < jsonSubpartCubesArr.size(); i++) {
						JsonObject cubeObject = jsonSubpartCubesArr.get(i).getAsJsonObject();

						JsonModelCube cube = new JsonModelCube();
						cube.deserialize(cubeObject);
						deserializeChildCubes(cubeObject, cube);

						subpartCubes.add(cube);
					}

					subparts_cubes.put(partName.toLowerCase(), subpartCubes);
				}
			}
		}

		this.setParentModel(parent);
		this.setSubPartCubes(subparts_cubes);
		this.setModelName(name);
		this.setTextureSize(texture_size);
		this.setColorTextureName(color_texture_name);
		this.setTextureName(texture_name);
		this.setModelCubes(cubes);
	}
	
	public void deserializeChildCubes(JsonObject parentObject, JsonModelCube parentCube) {
		ArrayList<JsonModelCube> childs = new ArrayList<>();
		if (parentObject.has("child_cubes")) {
			JsonArray arrayObject = JsonUtils.getJsonArray(parentObject, "child_cubes");
			
			for (int idx = 0; idx < arrayObject.size(); idx++) {
				JsonObject o2 = arrayObject.get(idx).getAsJsonObject();
				
				JsonModelCube childCube = new JsonModelCube();
				childCube.deserialize(o2);
				this.deserializeChildCubes(o2, childCube);
				
				childs.add(childCube);
			}
		}
		parentCube.setChildCubes(childs);
	}
}