package spyeedy.testmod.addonpack.model;

import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.util.JsonUtils;

public class JsonModelCube {
	
	public String cube_name;
	public int[] texture_offset;
	public float[] position;
	public float[] offset;
	public int[] dimensions;
	public float[] rotation_angles;
	public boolean mirror;
	public double[] custom_scale;
	public ArrayList<JsonModelCube> child_cubes;
	
	public JsonModelCube setCubeName(String cubeName) {
		this.cube_name = cubeName;
		return this;
	}

	public JsonModelCube setTextureOffset(int[] textureOffset) {
		this.texture_offset = textureOffset;
		return this;
	}

	public JsonModelCube setPosition(float[] position) {
		this.position = position;
		return this;
	}

	public JsonModelCube setOffset(float[] offset) {
		this.offset = offset;
		return this;
	}

	public JsonModelCube setDimensions(int[] dimensions) {
		this.dimensions = dimensions;
		return this;
	}
	
	public JsonModelCube setRotationAngles(float[] rotationAngles) {
		this.rotation_angles = rotationAngles;
		return this;
	}
	
	public JsonModelCube setMirror(boolean mirror) {
		this.mirror = mirror;
		return this;
	}

	public JsonModelCube setCustomScale(double[] custom_scale) {
		this.custom_scale = custom_scale;
		return this;
	}
	
	public JsonModelCube setChildCubes(ArrayList<JsonModelCube> childs) {
		this.child_cubes = childs;
		return this;
	}
	
	public void deserialize(JsonObject object) {
		this.setCubeName(JsonUtils.getString(object, "cube_name"));
		
		int[] texture_offset = new int[]{ JsonUtils.getJsonArray(object, "texture_offset").get(0).getAsInt(), JsonUtils.getJsonArray(object, "texture_offset").get(1).getAsInt() };
		this.setTextureOffset(texture_offset);
		
		float[] position = new float[] { JsonUtils.getJsonArray(object, "position").get(0).getAsFloat(), JsonUtils.getJsonArray(object, "position").get(1).getAsFloat(), JsonUtils.getJsonArray(object, "position").get(2).getAsFloat() };
		this.setPosition(position);
		
		float[] offset = new float[] { JsonUtils.getJsonArray(object, "offset").get(0).getAsFloat(), JsonUtils.getJsonArray(object, "offset").get(1).getAsFloat(), JsonUtils.getJsonArray(object, "offset").get(2).getAsFloat() };
		this.setOffset(offset);
		
		int[] dimensions = new int[]{ JsonUtils.getJsonArray(object, "dimensions").get(0).getAsInt(), JsonUtils.getJsonArray(object, "dimensions").get(1).getAsInt(), JsonUtils.getJsonArray(object, "dimensions").get(2).getAsInt() };
		this.setDimensions(dimensions);
		
		if (JsonUtils.hasField(object, "rotation_angles")) {
			float[] rotation_angles = new float[]{ JsonUtils.getJsonArray(object, "rotation_angles").get(0).getAsFloat(), JsonUtils.getJsonArray(object, "rotation_angles").get(1).getAsFloat(), JsonUtils.getJsonArray(object, "rotation_angles").get(2).getAsFloat() };
			this.setRotationAngles(rotation_angles);
		} else {
			this.setRotationAngles(null);
		}
		
		if (JsonUtils.hasField(object, "mirror")) {
			boolean mirror = JsonUtils.getBoolean(object, "mirror");
			this.setMirror(mirror);
		}
		else
			this.setMirror(false);

		if (JsonUtils.hasField(object, "custom_scale")) {
			JsonArray customScaleJsonArray = JsonUtils.getJsonArray(object, "custom_scale");
			double[] custom_scale = {customScaleJsonArray.get(0).getAsDouble(), customScaleJsonArray.get(1).getAsDouble(), customScaleJsonArray.get(2).getAsDouble()};
			this.setCustomScale(custom_scale);
		} else {
			this.setCustomScale(new double[]{1D, 1D, 1D});
		}
	}
}