package spyeedy.testmod.addonpack.model.client.model;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import spyeedy.testmod.addonpack.model.JsonModelCube;
import spyeedy.testmod.addonpack.model.util.JsonModelHelper;
import spyeedy.testmod.addonpack.model.JsonModelInfo;

public class ModelJSON extends ModelBase implements IJsonModel {
	
	private ArrayList<JsonModelCube> cubesList;
	private ModelRenderer[] cubes;
	private JsonModelInfo info;
	
	public ModelJSON(JsonModelInfo info) {
		this.textureWidth = info.texture_size[0];
		this.textureHeight = info.texture_size[1];
		this.info = info;

		this.cubesList = info.model_cubes;
		this.cubes = new ModelRenderer[cubesList.size()];
		JsonModelHelper.makeFromModelCubes(info, cubesList, cubes, this);
	}
	
	@Override
	public void render(float scale) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(info.texture));
		JsonModelHelper.renderFromModelCubes(scale, info, cubesList, cubes);
	}

	public ResourceLocation getTexture(){
		if (!info.texture.equalsIgnoreCase(""))
			return new ResourceLocation(info.texture);
		return null;
	}

	public ResourceLocation getColorTexture(){
		return new ResourceLocation(info.color_texture);
	}
}