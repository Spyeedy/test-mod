package spyeedy.testmod.addonpack.model.client.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import spyeedy.testmod.addonpack.model.JsonModelCube;
import spyeedy.testmod.addonpack.model.util.JsonModelHelper;
import spyeedy.testmod.addonpack.model.JsonModelInfo;
import spyeedy.testmod.addonpack.model.modeltype.ModelType;

import java.util.ArrayList;

public class ModelJsonBiped extends ModelBiped implements IJsonModel {
	
	private ArrayList<JsonModelCube> cubesList;
	private ModelRenderer[] cubes;
	private JsonModelInfo info;

	public ModelJsonBiped(JsonModelInfo info) {
		super(0, 0, info.texture_size[0], info.texture_size[1]);
		
		this.info = info;
		this.cubesList = info.model_cubes;
		this.cubes = new ModelRenderer[cubesList.size()];
		
		JsonModelHelper.makeFromModelCubes(info, cubesList, cubes, this);
		
		// Attach parts to various bipedal parts
		try {
			ModelType parentModelType = ModelType.MODEL_TYPE_REGISTRY.get(info.parent).newInstance();

			for (String part : parentModelType.getParts()) {
				ArrayList<JsonModelCube> subpartCubes = info.subpart_cubes.get(part);
				
				ModelRenderer parentPart = null;
				if (part.equalsIgnoreCase("HEAD")) {
					parentPart = bipedHead;
				} else if (part.equalsIgnoreCase("BODY")) {
					parentPart = bipedBody;
				} else if (part.equalsIgnoreCase("RIGHT_ARM")) {
					parentPart = bipedRightArm;
				} else if (part.equalsIgnoreCase("LEFT_ARM")) {
					parentPart = bipedLeftArm;
				} else if (part.equalsIgnoreCase("RIGHT_LEG")) {
					parentPart = bipedRightLeg;
				} else if (part.equalsIgnoreCase("LEFT_LEG")) {
					parentPart = bipedLeftLeg;
				}
				
				if (parentPart != null) {
					for (JsonModelCube cube : subpartCubes) {
						JsonModelHelper.addCubeToParentCube(this, cube, parentPart);
					}
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(info.texture));
		
		super.render(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		
		JsonModelHelper.renderFromModelCubes(scale, info, cubesList, cubes);
	}

	@Override
	public void render(float scale) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(info.texture));
		
		this.bipedBody.render(scale);
		this.bipedRightArm.render(scale);
		this.bipedLeftArm.render(scale);
		this.bipedRightLeg.render(scale);
		this.bipedLeftLeg.render(scale);
		this.bipedHead.render(scale);
		
		JsonModelHelper.renderFromModelCubes(scale, info, cubesList, cubes);
	}
}