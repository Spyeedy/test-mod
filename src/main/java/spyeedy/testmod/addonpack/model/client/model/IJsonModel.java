package spyeedy.testmod.addonpack.model.client.model;

public interface IJsonModel {

	void render(float scale);
}