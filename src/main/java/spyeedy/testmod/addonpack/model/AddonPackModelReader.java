package spyeedy.testmod.addonpack.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lucraft.mods.lucraftcore.addonpacks.AddonPackReadEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.commons.io.FilenameUtils;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.addonpack.model.client.model.IJsonModel;
import spyeedy.testmod.addonpack.model.client.model.ModelJSON;
import spyeedy.testmod.addonpack.model.modeltype.ModelType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;

@EventBusSubscriber
public class AddonPackModelReader {

	public static final LinkedHashMap<String, IJsonModel> MODELS = new LinkedHashMap<>();
	
	@SubscribeEvent
	public static void onRead(AddonPackReadEvent e) {
		if (e.getDirectory().equals("constructs") && e.getResourceLocation().getPath().contains("models\\") && FilenameUtils.getExtension(e.getFileName()).equalsIgnoreCase("json")) {			
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(e.getInputStream(), StandardCharsets.UTF_8));
				JsonObject jsonobject = new JsonParser().parse(reader).getAsJsonObject();
				JsonModelInfo info = new JsonModelInfo();
				info.deserialize(jsonobject);

				String id = e.getResourceLocation().getNamespace() + ":" + info.name;
				if (info.parent != null) {
					ModelType modelType = ModelType.MODEL_TYPE_REGISTRY.get(info.parent).newInstance();
					IJsonModel model = modelType.getModelClass().getConstructor(JsonModelInfo.class).newInstance(info);
					MODELS.put(id, model);
				} else {
					MODELS.put(id, new ModelJSON(info));
				}
			} catch (Exception e2) {
				TestMod.LOGGER.error("Wasn't able to read model '" + e.getFileName() + "' in addon pack '" + e.getPackFile().getName() + "': " + e2.getMessage());
			}
		}
	}
}