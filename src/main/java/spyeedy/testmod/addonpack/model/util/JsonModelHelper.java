package spyeedy.testmod.addonpack.model.util;

import java.util.ArrayList;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import spyeedy.testmod.addonpack.model.JsonModelCube;
import spyeedy.testmod.addonpack.model.JsonModelInfo;

public class JsonModelHelper {	
	public static void makeFromModelCubes(JsonModelInfo info, ArrayList<JsonModelCube> cubesList, ModelRenderer[] cubes, ModelBase model) {
		for (JsonModelCube cube : cubesList) {
			if (cube != null) {
				int index = cubesList.indexOf(cube);
				cubes[index] = new ModelRenderer(model, cube.texture_offset[0], cube.texture_offset[1]);
				cubes[index].addBox(cube.offset[0], cube.offset[1], cube.offset[2], cube.dimensions[0], cube.dimensions[1], cube.dimensions[2]);
				cubes[index].setRotationPoint(cube.position[0], cube.position[1], cube.position[2]);
				cubes[index].mirror = cube.mirror;
				if (cube.rotation_angles != null) {
					cubes[index].rotateAngleX = (float) (cube.rotation_angles[0] * (Math.PI / 180));
					cubes[index].rotateAngleY = (float) (cube.rotation_angles[1] * (Math.PI / 180));
					cubes[index].rotateAngleZ = (float) (cube.rotation_angles[2] * (Math.PI / 180));
				}
				if (cube.child_cubes.size() > 0) {
					makeChildCubes(cube, model, cubes[index]);
				}
			}
		}
	}
	
	public static void makeChildCubes(JsonModelCube parentInfo, ModelBase model, ModelRenderer cuboid) {		
		if (parentInfo.child_cubes.size() > 0) {
			for (JsonModelCube info : parentInfo.child_cubes) {
				ModelRenderer c = new ModelRenderer(model, info.texture_offset[0], info.texture_offset[1]);
				c.addBox(info.offset[0], info.offset[1], info.offset[2], info.dimensions[0], info.dimensions[1], info.dimensions[2]);
				c.setRotationPoint(info.position[0], info.position[1], info.position[2]);
				c.mirror = info.mirror;
				if (info.rotation_angles != null) {
					c.rotateAngleX = (float) (info.rotation_angles[0] * (Math.PI / 180));
					c.rotateAngleY = (float) (info.rotation_angles[1] * (Math.PI / 180));
					c.rotateAngleZ = (float) (info.rotation_angles[2] * (Math.PI / 180));
				}
				makeChildCubes(info, model, c);
				cuboid.addChild(c);
			}
		}
	}
	
	public static void addCubeToParentCube(ModelBase model, JsonModelCube cube, ModelRenderer parentCube) {
		ModelRenderer cuboid = new ModelRenderer(model, cube.texture_offset[0], cube.texture_offset[1]);
		
		cuboid.addBox(cube.offset[0], cube.offset[1], cube.offset[2], cube.dimensions[0], cube.dimensions[1], cube.dimensions[2]);
		cuboid.setRotationPoint(cube.position[0], cube.position[1], cube.position[2]);
		cuboid.mirror = cube.mirror;
		if (cube.rotation_angles != null) {
			cuboid.rotateAngleX = (float) (cube.rotation_angles[0] * (Math.PI / 180));
			cuboid.rotateAngleY = (float) (cube.rotation_angles[1] * (Math.PI / 180));
			cuboid.rotateAngleZ = (float) (cube.rotation_angles[2] * (Math.PI / 180));
		}
		if (cube.child_cubes.size() > 0) {
			JsonModelHelper.makeChildCubes(cube, model, cuboid);
		}
		
		parentCube.addChild(cuboid);
	}
	
	public static void renderFromModelCubes(float scale, JsonModelInfo info, ArrayList<JsonModelCube> cubesList, ModelRenderer[] cubes) {
		for (int cube = 0; cube < cubesList.size(); cube++) {
			ModelRenderer modelCube = cubes[cube];

			GlStateManager.pushMatrix();
			GlStateManager.translate(modelCube.offsetX, modelCube.offsetY, modelCube.offsetZ);
			GlStateManager.translate(modelCube.rotationPointX * scale, modelCube.rotationPointY * scale, modelCube.rotationPointZ * scale);
			GlStateManager.scale(cubesList.get(cube).custom_scale[0], cubesList.get(cube).custom_scale[1], cubesList.get(cube).custom_scale[2]);
			GlStateManager.translate(-modelCube.offsetX, -modelCube.offsetY, -modelCube.offsetZ);
			GlStateManager.translate(-modelCube.rotationPointX * scale, -modelCube.rotationPointY * scale, -modelCube.rotationPointZ * scale);
			modelCube.render(scale);
			GlStateManager.popMatrix();
		}
	}
}