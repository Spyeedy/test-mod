package spyeedy.testmod.creativetabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import spyeedy.testmod.blocks.TestBlocks;

public class CreativeTabTestMod extends CreativeTabs {

	public CreativeTabTestMod() {
		super("tabTestMod");
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(TestBlocks.test_storage);
	}
}