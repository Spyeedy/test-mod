package spyeedy.testmod.suitset;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.inventory.EntityEquipmentSlot;

public class SuitSetRingSuitUp extends TMSuitSet {

	public SuitSetRingSuitUp() {
		super("ring_suit_up");
	}

	@Override
	public boolean hasGlowyThings(EntityLivingBase entity, EntityEquipmentSlot slot) {
		/*if (slot == EntityEquipmentSlot.CHEST)
			return true;*/
		return super.hasGlowyThings(entity, slot);
	}
}