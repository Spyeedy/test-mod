package spyeedy.testmod.suitset;

import lucraft.mods.lucraftcore.superpowers.suitsets.RegisterSuitSetEvent;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import spyeedy.testmod.TestMod;

@Mod.EventBusSubscriber(modid = TestMod.MODID)
public class TMSuitSet extends SuitSet {

	public static final TMSuitSet RING_SUIT_UP = new SuitSetRingSuitUp();

	@SubscribeEvent
	public static void onRegisterSuitSet(RegisterSuitSetEvent e) {
		e.register(RING_SUIT_UP);
	}

	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> e) {
		for (ResourceLocation sets : SuitSet.REGISTRY.getKeys()) {
			if (SuitSet.REGISTRY.getObject(sets) instanceof TMSuitSet) {
				SuitSet.REGISTRY.getObject(sets).registerItems(e);
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public static void onRegisterModels(ModelRegistryEvent e) {
		for (ResourceLocation sets : SuitSet.REGISTRY.getKeys()) {
			if (SuitSet.REGISTRY.getObject(sets) instanceof TMSuitSet) {
				SuitSet.REGISTRY.getObject(sets).registerModels();
			}
		}
	}

	// ------------------------------------------------------------------

	public TMSuitSet(String name) {
		super(name);
		this.setRegistryName(TestMod.MODID, name);
	}
}