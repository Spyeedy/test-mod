package spyeedy.testmod.network;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.relauncher.Side;
import spyeedy.testmod.network.client.MessageUpdateTypes;
import spyeedy.testmod.network.server.MessageOpenExtraSlots;
import spyeedy.testmod.network.server.MessageSyncItemData;
import spyeedy.testmod.network.server.MessageSyncUpdateTypes;

import static spyeedy.testmod.TestMod.networkWrapper;

public class PacketDispatcher {

	public static void registerPackets() {
		int id = 0;
		
		registerClient(MessageUpdateTypes.Handler.class, MessageUpdateTypes.class, id++);
		registerServer(MessageSyncUpdateTypes.Handler.class, MessageSyncUpdateTypes.class, id++);
		
		registerServer(MessageOpenExtraSlots.Handler.class, MessageOpenExtraSlots.class, id++);
		registerServer(MessageSyncItemData.Handler.class, MessageSyncItemData.class, id++);
	}
	
	private static <REQ extends IMessage, REPLY extends IMessage> void registerClient(Class<? extends IMessageHandler<REQ, REPLY>> handler, Class<REQ> message, int id) {
		networkWrapper.registerMessage(handler, message, id, Side.CLIENT);
	}
	
	private static <REQ extends IMessage, REPLY extends IMessage> void registerServer(Class<? extends IMessageHandler<REQ, REPLY>> handler, Class<REQ> message, int id) {
		networkWrapper.registerMessage(handler, message, id, Side.SERVER);
	}
	
	public static final void sendToServer(IMessage message) {
		networkWrapper.sendToServer(message);
	}
	
	public static final void sendTo(IMessage message, EntityPlayerMP player) {
		networkWrapper.sendTo(message, player);
	}
}