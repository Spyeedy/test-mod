package spyeedy.testmod.network.server;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import spyeedy.testmod.capability.itemdata.CapabilityItemData;
import spyeedy.testmod.capability.itemdata.IItemData;
import spyeedy.testmod.items.json.ItemModels;

public class MessageSyncItemData implements IMessage {

	private ItemDataInfoType info;
	private int amount;
	
	public MessageSyncItemData() {}
	
	public MessageSyncItemData(ItemDataInfoType info) {
		this(info, 0);
	}
	
	public MessageSyncItemData(ItemDataInfoType info, int amount) {
		this.info = info;
		this.amount = amount;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.amount = buf.readInt();
		this.info = ItemDataInfoType.values()[buf.readInt()];
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(amount);
		buf.writeInt(this.info.ordinal());
	}
	
	public static class Handler extends AbstractServerMessageHandler<MessageSyncItemData> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSyncItemData message, MessageContext ctx) {
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
					ItemStack stack = player.getHeldItemMainhand();
					
					if (!stack.isEmpty() && stack.getItem() instanceof ItemModels) {
						IItemData augment = stack.getCapability(CapabilityItemData.ITEM_DATA_CAPABILITY, null);
						
						switch (message.info) {
						case INCREASE_COINS:
							augment.adjustCoins(message.amount, true);
							break;
						case DECREASE_COINS:
							augment.adjustCoins(message.amount, false);
							break;
						}
					}
			});
			return null;
		}
	}
	
	public enum ItemDataInfoType {
		INCREASE_COINS, DECREASE_COINS;
	}
}