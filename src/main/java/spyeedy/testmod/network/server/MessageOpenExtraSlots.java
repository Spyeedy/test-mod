package spyeedy.testmod.network.server;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import spyeedy.testmod.TestMod;

public class MessageOpenExtraSlots implements IMessage {
	
	public MessageOpenExtraSlots() {}

	@Override
	public void fromBytes(ByteBuf buf) {}

	@Override
	public void toBytes(ByteBuf buf) {}

	public static class Handler implements IMessageHandler<MessageOpenExtraSlots, IMessage> {

		@Override
		public IMessage onMessage(MessageOpenExtraSlots message, MessageContext ctx) {

			ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
					ctx.getServerHandler().player.openGui(TestMod.instance, TestMod.EXTRA_SLOTS_ID, ctx.getServerHandler().player.world, 0, 0, 0);
			});

			return null;
		}
	}
}