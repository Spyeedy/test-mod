package spyeedy.testmod.network.server;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import spyeedy.testmod.capability.types.TDefaultImplementation;
import spyeedy.testmod.capability.types.CapabilityTypes;

public class MessageSyncUpdateTypes implements IMessage {

	private NBTTagCompound nbt;
	
	public MessageSyncUpdateTypes() {}
	
	public MessageSyncUpdateTypes(TDefaultImplementation cap) {
		this.nbt = cap.serializeNBT();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeTag(buf, nbt);
	}

	public static class Handler implements IMessageHandler<MessageSyncUpdateTypes, IMessage> {

		@Override
		public IMessage onMessage(MessageSyncUpdateTypes message, MessageContext ctx) {
			WorldServer thread = (WorldServer) ctx.getServerHandler().player.world;
			thread.addScheduledTask(() -> {
				TDefaultImplementation data = (TDefaultImplementation) ctx.getServerHandler().player.getCapability(CapabilityTypes.TYPES_CAP, null);
				if (data != null) data.deserializeNBT(message.nbt);
			});
			return null;
		}
	}
}
