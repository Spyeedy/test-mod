package spyeedy.testmod.network.client;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import spyeedy.testmod.capability.types.TDefaultImplementation;
import spyeedy.testmod.capability.types.CapabilityTypes;

public class MessageUpdateTypes implements IMessage {

	private NBTTagCompound nbt;
	
	public MessageUpdateTypes(TDefaultImplementation cap) {
		nbt = cap.serializeNBT();
	}

	public MessageUpdateTypes() {}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeTag(buf, nbt);
	}

	public static class Handler implements IMessageHandler<MessageUpdateTypes, IMessage> {

		@Override
		public IMessage onMessage(MessageUpdateTypes message, MessageContext ctx) {
			IThreadListener thread = Minecraft.getMinecraft();
			thread.addScheduledTask(() -> {
				TDefaultImplementation cap = (TDefaultImplementation) Minecraft.getMinecraft().player.getCapability(CapabilityTypes.TYPES_CAP, null);
				if (cap != null) cap.deserializeNBT(message.nbt);
			});
			return null;
		}
	}
}