package spyeedy.testmod.blocks;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.util.ModelHelper;

@EventBusSubscriber
public class TestBlocks {

	public static final Block test_storage = new BlockTestStorage();
	
	public static final Block oak_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.OAK);
	public static final Block spruce_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.SPRUCE);
	public static final Block birch_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.BIRCH);
	public static final Block jungle_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.JUNGLE);
	public static final Block acacia_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.ACACIA);
	public static final Block dark_oak_glass_display = new BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants.DARK_OAK);
	
	//public static final Block glass_display = new BlockGlassDisplayVariant();

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(
					test_storage,
					oak_glass_display,
					spruce_glass_display,
					birch_glass_display,
					jungle_glass_display,
					acacia_glass_display,
					dark_oak_glass_display
				);
	}
	
	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		final ItemBlock[] itemBlocks = {
			new ItemBlock(test_storage),
			new ItemBlock(oak_glass_display),
			new ItemBlock(spruce_glass_display),
			new ItemBlock(birch_glass_display),
			new ItemBlock(jungle_glass_display),
			new ItemBlock(acacia_glass_display),
			new ItemBlock(dark_oak_glass_display)
		};
		
		for (final ItemBlock item : itemBlocks) {
			event.getRegistry().register(item.setRegistryName(item.getBlock().getRegistryName()));
		}
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event) {
		ModelHelper.registerBlock(test_storage);
		ModelHelper.registerBlock(oak_glass_display);
		ModelHelper.registerBlock(spruce_glass_display);
		ModelHelper.registerBlock(birch_glass_display);
		ModelHelper.registerBlock(jungle_glass_display);
		ModelHelper.registerBlock(acacia_glass_display);
		ModelHelper.registerBlock(dark_oak_glass_display);
	}
}