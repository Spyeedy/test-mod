package spyeedy.testmod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import spyeedy.testmod.TestMod;

public class BlockGlassDisplayVariant extends Block {
	
	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}
	
	// ------------------------------------------------=
	
	public BlockGlassDisplayVariant(BlockGlassDisplayVariant.Variants variant) {
		super(Material.WOOD);
		this.setRegistryName(variant.getName() + "_glass_display");
		this.setTranslationKey(variant.getName() + "_glass_display");
		this.setCreativeTab(TestMod.tabTestMod);
	}
	
	/*public static final PropertyEnum VARIANT = PropertyEnum.create("variant", BlockGlassDisplayVariant.Variants.class);
			
	public BlockGlassDisplayVariant() {
		super(Material.WOOD);
		this.setRegistryName("glass_display");
		this.setUnlocalizedName("glass_display");
		this.setCreativeTab(TestMod.tabTestMod);
	}*/
	
	/*@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, VARIANT);
	}*/
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	public enum Variants implements IStringSerializable {
		OAK, SPRUCE, BIRCH, JUNGLE, ACACIA, DARK_OAK;
		
		@Override
		public String getName() {
			return this.toString().toLowerCase();
		}
	}
}