package spyeedy.testmod.blocks;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.IItemHandler;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.tileentity.TileEntityTestStorage;

public class BlockTestStorage extends BlockSpyTM implements ITileEntityProvider {

	public BlockTestStorage() {
		super("test_storage");
		GameRegistry.registerTileEntity(TileEntityTestStorage.class, new ResourceLocation(TestMod.MODID, "test_storage"));
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		TileEntity te = worldIn.getTileEntity(pos);
		if (!worldIn.isRemote) {
			if(te instanceof TileEntityTestStorage && !playerIn.isSneaking()) {
				playerIn.openGui(TestMod.instance, TestMod.STORAGE_ID, worldIn, pos.getX(), pos.getY(), pos.getZ());
			}
			return true;
		}
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityTestStorage();
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		TileEntity te = worldIn.getTileEntity(pos);
		if (worldIn.getTileEntity(pos) instanceof TileEntityTestStorage) {
			IItemHandler inv = ((TileEntityTestStorage)worldIn.getTileEntity(pos)).inventory;
			for (int i = 0; i < inv.getSlots(); i++) {
				ItemStack stack = inv.getStackInSlot(i);
				
				if (!stack.isEmpty()) {
					InventoryHelper.spawnItemStack(worldIn, pos.getX(), pos.getY(), pos.getZ(), stack);
				}
			}
		}
		super.breakBlock(worldIn, pos, state);
	}
}