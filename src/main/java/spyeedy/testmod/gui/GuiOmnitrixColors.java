package spyeedy.testmod.gui;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.input.Keyboard;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiCheckBox;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.capability.types.CapabilityTypes;
import spyeedy.testmod.capability.types.ITypes;
import spyeedy.testmod.capability.types.OmnitrixColorTypes;
import spyeedy.testmod.items.TMItems;

public class GuiOmnitrixColors extends GuiScreen {

	private final EntityPlayer player;

	private ITypes colors;
	
	public GuiOmnitrixColors(EntityPlayer player) {
		this.player = player;
		this.colors = player.getCapability(CapabilityTypes.TYPES_CAP, null);
	}
	
	// ----------------------------------------------------------------------------------------------------------------------
	
	private static final ResourceLocation GUI = new ResourceLocation(TestMod.MODID, "textures/gui/colors_customizer.png");

	private int xSize = 256;
	private int ySize = 205;

	private int modelViewerWidth = 45;
	
	public OmnitrixColorTypes type;
	private Map<OmnitrixColorTypes, GuiButton> typeButtons = new HashMap<>();
	
	private GuiTextField color;
	
	private GuiButton btnSave;
	private GuiButton btnReset;

	private GuiCheckBox enableRechargeModeColor;
	
	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;
		
		this.color = new GuiTextField(0, fontRenderer, i + 10, j + 95, 110, 20);
		this.color.setMaxStringLength(6);
		
		type = OmnitrixColorTypes.FACE;
		
		GuiButton btnFACE = new GuiButtonExt(3, i + 6, j + 18, 80, 18, OmnitrixColorTypes.FACE.getName());
		typeButtons.put(OmnitrixColorTypes.FACE, btnFACE);
		this.buttonList.add(btnFACE);
		
		GuiButton btnRING = new GuiButtonExt(4, i + 90, j + 18, 80, 18, OmnitrixColorTypes.RING.getName());
		typeButtons.put(OmnitrixColorTypes.RING, btnRING);
		this.buttonList.add(btnRING);
		
		GuiButton btnBODY = new GuiButtonExt(5, i + 6, j + 40, 80, 18, OmnitrixColorTypes.BODY.getName());
		typeButtons.put(OmnitrixColorTypes.BODY, btnBODY);
		this.buttonList.add(btnBODY);
		
		GuiButton btnWATCH_STRAP = new GuiButtonExt(6, i + 90, j + 40, 80, 18, OmnitrixColorTypes.WATCH_STRAP.getName());
		typeButtons.put(OmnitrixColorTypes.WATCH_STRAP, btnWATCH_STRAP);
		this.buttonList.add(btnWATCH_STRAP);

		/*GuiButton btnRechargeMode = new GuiButtonExt(7, i + 6, j + 62, 164, 18, OmnitrixColorTypes.RechargeMode.getName());
		typeButtons.put(OmnitrixColorTypes.RechargeMode, btnRechargeMode);
		this.buttonList.add(btnRechargeMode);*/

		updateTypeButton(type);
		
		this.buttonList.add(btnSave = new GuiButtonExt(8, i + xSize/2 - 45 - 8 - modelViewerWidth, j + 185, 45, 14, "Save"));
		this.buttonList.add(btnReset = new GuiButtonExt(9, i + xSize/2 + 8 - modelViewerWidth, j + 185, 45, 14, "Reset"));
	}

	@Override
	public void updateScreen() {
		super.updateScreen();
		if (this.color.isFocused()) this.color.updateCursorCounter();
		
		btnSave.enabled = color.getText().length() == 6;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.color(1F, 1F, 1F, 1F);
		mc.getTextureManager().bindTexture(GUI);
		this.drawTexturedModalRect(i, j, 0, 0, xSize, ySize);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
		
		String title = "OG Omnitrix Customizer";
		fontRenderer.drawString(title, i + xSize/2 - fontRenderer.getStringWidth(title)/2 - modelViewerWidth, j + 4, Color.WHITE.getRGB(), true);
		
		this.color.drawTextBox();
		
		LCRenderHelper.drawRect(i + 130, j + 94, i + 152, j + 182, 0, 0, 0, 1);
		
		String hex = this.color.getText();
		if (hex.length() == 6) {
			String redHex = hex.substring(0, 2);
			String greenHex = hex.substring(2, 4);
			String blueHex = hex.substring(4);
			
			int redDeci = hexToDecimal(redHex);
			int greenDeci = hexToDecimal(greenHex);
			int blueDeci = hexToDecimal(blueHex);
			
			LCRenderHelper.drawRect(i + 131, j + 95, i + 151, j + 181, (float) redDeci/255, (float) greenDeci/255, (float) blueDeci/255, 1);
		}
		
		fontRenderer.drawString("Color", i + 10, j + 85, Color.GRAY.getRGB());
		
		super.drawScreen(mouseX, mouseY, partialTicks);

		renderOmnitrixModel();
	}

	private void updateTypeButton(OmnitrixColorTypes type) {
		this.type = type;
		
		for (OmnitrixColorTypes types : typeButtons.keySet()) {
			if (types == type)
				typeButtons.get(types).enabled = false;
			else
				typeButtons.get(types).enabled = true;
		}
		
		switch (type) {
		case BODY:
			this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.BODY)));
			break;
		case FACE:
			this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.FACE)));
			break;
		case RING:
			this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.RING)));
			break;
		case WATCH_STRAP:
			this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.WATCH_STRAP)));
			break;
		/*case RechargeMode:
			this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.RechargeMode)));
			break;*/
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		if (button.id == 3) {
			updateTypeButton(OmnitrixColorTypes.FACE);
		} else if (button.id == 4) {
			updateTypeButton(OmnitrixColorTypes.RING);
		} else if (button.id == 5) {
			updateTypeButton(OmnitrixColorTypes.BODY);
		} else if (button.id == 6) {
			updateTypeButton(OmnitrixColorTypes.WATCH_STRAP);
		}/* else if (button.id == 7)
			updateTypeButton(OmnitrixColorTypes.RechargeMode);*/
		
		if (button == btnSave) {
			if (type == OmnitrixColorTypes.FACE)
				colors.setColor(OmnitrixColorTypes.FACE, this.color.getText());
			else if (type == OmnitrixColorTypes.RING)
				colors.setColor(OmnitrixColorTypes.RING, this.color.getText());
			else if (type == OmnitrixColorTypes.BODY)
				colors.setColor(OmnitrixColorTypes.BODY, this.color.getText());
			else if (type == OmnitrixColorTypes.WATCH_STRAP)
				colors.setColor(OmnitrixColorTypes.WATCH_STRAP, this.color.getText());
			/*else if (type == OmnitrixColorTypes.RechargeMode)
				colors.setColor(OmnitrixColorTypes.RechargeMode, this.color.getText());*/
		}
		else if (button == btnReset) {
			if (type == OmnitrixColorTypes.FACE) {
				colors.resetColors(OmnitrixColorTypes.FACE);
				this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.FACE)));
			}
			else if (type == OmnitrixColorTypes.RING) {
				colors.resetColors(OmnitrixColorTypes.RING);
				this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.RING)));
			}
			else if (type == OmnitrixColorTypes.BODY) {
				colors.resetColors(OmnitrixColorTypes.BODY);
				this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.BODY)));
			}
			else if (type == OmnitrixColorTypes.WATCH_STRAP) {
				colors.resetColors(OmnitrixColorTypes.WATCH_STRAP);
				this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.WATCH_STRAP)));
			}
			/*else if (type == OmnitrixColorTypes.RechargeMode) {
				colors.resetColors(OmnitrixColorTypes.RechargeMode);
				this.color.setText(decimalToHex(colors.getColor(OmnitrixColorTypes.RechargeMode)));
			}*/
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.color.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		int[] allowedKeys = { Keyboard.KEY_BACK, Keyboard.KEY_DELETE, Keyboard.KEY_LEFT, Keyboard.KEY_RIGHT, Keyboard.KEY_HOME };
		
		if (Character.isDigit(typedChar) || Character.isAlphabetic(typedChar) || keyCode == allowedKeys[0] || keyCode == allowedKeys[1] || keyCode == allowedKeys[2] || keyCode == allowedKeys[3] || keyCode == allowedKeys[4]) {
			if (color.isFocused()) {
				super.keyTyped(typedChar, keyCode);
				color.textboxKeyTyped(typedChar, keyCode);
			}
		}

		if (keyCode == Keyboard.KEY_RETURN) {
			if (color.isFocused() && color.getText().length() == 6) {
				if (type == OmnitrixColorTypes.FACE)
					colors.setColor(OmnitrixColorTypes.FACE, this.color.getText());
				else if (type == OmnitrixColorTypes.RING)
					colors.setColor(OmnitrixColorTypes.RING, this.color.getText());
				else if (type == OmnitrixColorTypes.BODY)
					colors.setColor(OmnitrixColorTypes.BODY, this.color.getText());
				else if (type == OmnitrixColorTypes.WATCH_STRAP)
					colors.setColor(OmnitrixColorTypes.WATCH_STRAP, this.color.getText());
				/*else if (type == OmnitrixColorTypes.RechargeMode)
					colors.setColor(OmnitrixColorTypes.RechargeMode, this.color.getText());*/
			}
		}
		
		if (GuiScreen.isKeyComboCtrlA(keyCode)) {
			color.setSelectionPos(color.getMaxStringLength());
		}
		
		if (!color.isFocused()) {
			super.keyTyped(typedChar, keyCode);
		}
	}

	private void renderOmnitrixModel() {
		int x = (this.width - this.xSize)/2 - (this.width == 427 ? 26 : 47);
		int y = (this.height - ySize)/2 + 2 - (this.height == 240 ? 0 : 5);

		double scale = 4.5;
		ItemStack omnitrix = new ItemStack(TMItems.OG_OMNITRIX);
		
		GlStateManager.pushMatrix();
		RenderHelper.enableGUIStandardItemLighting();
		//GlStateManager.rotate((player.ticksExisted + LCRenderHelper.renderTick) * 2, 0, 1, 0); // Rotation

		
		GlStateManager.scale(scale, scale, scale);
		itemRender.renderItemIntoGUI(omnitrix, x, y);
		GlStateManager.scale(1, 1, 1);
		
		RenderHelper.disableStandardItemLighting();
		GlStateManager.popMatrix();
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	// ------------------------------ HELPER METHODS -------------------------------------------
	
	private int hexToDecimal(String hex) {
		String hexTable = "0123456789ABCDEF";
		hex = hex.toUpperCase();
		int val = 0;
		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(i);
			int hexVal = hexTable.indexOf(c);
			
			int power = hex.length() - 1 - i;
			val += hexVal * Math.pow(16, power);
		}
		return val;
	}
	
	private String decimalToHex(int decimal) {
		String hex = Integer.toHexString(decimal);
		if (hex.length() != 6) {
			int numOf0 = 6 - hex.length();
			for (int i = 0; i < numOf0; i++) {
				hex = "0" + hex;
			}
		}
		return hex;
	}
}