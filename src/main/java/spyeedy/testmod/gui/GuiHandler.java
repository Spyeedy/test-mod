package spyeedy.testmod.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.capability.extraslot.CapabilityExtraSlot;
import spyeedy.testmod.gui.container.ContainerExtraSlot;
import spyeedy.testmod.gui.container.ContainerInventoryItem;
import spyeedy.testmod.gui.container.ContainerTestStorage;
import spyeedy.testmod.tileentity.TileEntityTestStorage;

public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = player.world.getTileEntity(new BlockPos(x, y, z));
		switch (ID) {
			case TestMod.STORAGE_ID:
				if (te instanceof TileEntityTestStorage)
					return new ContainerTestStorage(player.inventory, (TileEntityTestStorage) te);
			case TestMod.INVENTORY_ITEM_ID:
				return new ContainerInventoryItem(player.inventory, player.getHeldItemMainhand().getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null));
			case TestMod.EXTRA_SLOTS_ID:
				return new ContainerExtraSlot(player.inventory, player.getCapability(CapabilityExtraSlot.EXTRA_SLOT_CAPABILITY, null));
			default:
				return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = player.world.getTileEntity(new BlockPos(x, y, z));
		switch (ID) {
			case TestMod.STORAGE_ID:
				if (te instanceof TileEntityTestStorage)
					return new GuiTestStorage(player.inventory, (TileEntityTestStorage) te);
			case TestMod.INVENTORY_ITEM_ID:
				return new GuiInventoryItem(player.inventory, player.getHeldItemMainhand().getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null));
			case TestMod.EXTRA_SLOTS_ID:
				return new GuiExtraSlot(player.inventory, player.getCapability(CapabilityExtraSlot.EXTRA_SLOT_CAPABILITY, null));
			default:
				return null;
		}
	}
}