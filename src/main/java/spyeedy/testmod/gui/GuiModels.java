package spyeedy.testmod.gui;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.addonpack.model.AddonPackModelReader;
import spyeedy.testmod.addonpack.model.client.model.IJsonModel;

import java.io.IOException;

public class GuiModels extends GuiScreen {

	private final EntityPlayer player;

	private int modelId;
	private ItemStack stack;
	
	public GuiModels(EntityPlayer player) {
		this.player = player;
		this.stack = player.getHeldItemMainhand();
	}
	
	// ----------------------------------------------------------------------------------------------------------------------
	
	private static final ResourceLocation GUI = new ResourceLocation(TestMod.MODID, "textures/gui/bank.png");
	
	private int xSize = 176;
	private int ySize = 190;
	
	@Override
	public void initGui() {
		super.initGui();
		
		int x = (this.width - this.xSize)/2;
		int y = (this.height - this.ySize)/2;

		// TODO
		modelId = -1;
		System.out.println("Selected Model:" + modelId);
		
		for (int model = 0; model < AddonPackModelReader.MODELS.size(); model++) {
			String name = (String) AddonPackModelReader.MODELS.keySet().toArray()[model];
			
			GuiButton btn = new GuiButtonExt(model+1, x + 5, y + 10 + 20 * model, mc.fontRenderer.getStringWidth(name) + 10, 18, name);
			
			this.buttonList.add(btn);
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;
		
		mc.getTextureManager().bindTexture(GUI);
		this.drawTexturedModalRect(i, j, 0, 0, xSize, ySize);
		
		super.drawScreen(mouseX, mouseY, partialTicks);

		if (this.modelId > -1) {
			renderModel();
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);

		for (int i = 0; i <= AddonPackModelReader.MODELS.size(); i++)
			if (button.id == i+1)
				updateModelButton(i);
	}
	
	private void updateModelButton(int modelId) {
		this.modelId = modelId;
		
		for (GuiButton btn : buttonList) {
			if (btn.id != 0) {
				btn.enabled = btn.id - 1 != modelId;
			}
		}
	}
	
	private void renderModel() {
		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		GlStateManager.pushMatrix();
		GlStateManager.color(1, 1, 1, 1);
		GlStateManager.translate(x + 100, y + 102, 50);
		GlStateManager.scale(-40, 40, 40);
		GlStateManager.rotate(-(player.ticksExisted + LCRenderHelper.renderTick) * 2, 0, 1, 0); // Rotation

		((IJsonModel) AddonPackModelReader.MODELS.values().toArray()[modelId]).render(0.0625F);

		GlStateManager.color(1, 1, 1, 1);
		GlStateManager.popMatrix();
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
}