package spyeedy.testmod.gui;

import java.awt.Color;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.IItemHandler;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.gui.container.ContainerInventoryItem;

public class GuiInventoryItem extends GuiContainer {

	private static final ResourceLocation TEXTURE = new ResourceLocation(TestMod.MODID + ":textures/gui/storage.png");
	
	public GuiInventoryItem(InventoryPlayer playerInv, IItemHandler itemInv) {
		super(new ContainerInventoryItem(playerInv, itemInv));
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		this.fontRenderer.drawString("Storage", xSize/2 - 35, 6, Color.BLUE.getRGB());
		this.fontRenderer.drawString("Inventory", 8, this.ySize - 94, Color.BLUE.getRGB());
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		int x = (width - xSize)/2;
		int y = (height - ySize)/2;
		
		this.mc.getTextureManager().bindTexture(TEXTURE);
		this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
	}
}