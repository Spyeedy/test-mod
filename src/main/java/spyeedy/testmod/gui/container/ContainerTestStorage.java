package spyeedy.testmod.gui.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import spyeedy.testmod.tileentity.TileEntityTestStorage;

public class ContainerTestStorage extends Container {
	
	/* SLOT IDS
	 * 0 - 11, 1st Cabinet Inventory
	 * 12 - 23, 2nd Cabinet Inventory
	 * 24 - 50, Player Inventory
	 * 51 - 59, Hotbar Inventory
	 */
	
	public ContainerTestStorage(IInventory playerInv, TileEntityTestStorage te) {
		IItemHandler inv = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
		
		// Cabinet Left Slots
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 4; x++) {
				this.addSlotToContainer(new SlotItemHandler(inv, x + y * 4, x * 18 + 8, y * 18 + 17) {
					@Override
					public void onSlotChanged() {
						te.markDirty();
					}
				});
			}
		}
		
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 4; x++) {
				this.addSlotToContainer(new SlotItemHandler(inv, x + y * 4 + 12, x * 18 + 98, y * 18 + 17) {
					@Override
					public void onSlotChanged() {
						te.markDirty();
					}
				});
			}
		}
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, j * 18 + 8, i * 18 + 84));
			}
		}

		for (int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(playerInv, i, i * 18 + 8, 142));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = inventorySlots.get(index);
	
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
	
			if (index >= 0 && index <= 11) {
				if (!this.mergeItemStack(itemstack1, 51, 60, false) && !this.mergeItemStack(itemstack1, 24, 51, false) && !this.mergeItemStack(itemstack1, 12, 24, false)) {
					return ItemStack.EMPTY;
				}
			} else if (index >= 12 && index <= 23) {
				if (!this.mergeItemStack(itemstack1, 51, 60, false) && !this.mergeItemStack(itemstack1, 24, 51, false) && !this.mergeItemStack(itemstack1, 0, 12, false)) {
					return ItemStack.EMPTY;
				}
			} else if (index >= 24 && index <= 50) {
				if (!this.mergeItemStack(itemstack1, 0, 24, false) && !this.mergeItemStack(itemstack1, 51, 60, false)) {
					return ItemStack.EMPTY;
				}
			} else if (index >= 51 && index <= 59) {
				if (!this.mergeItemStack(itemstack1, 0, 51, false)) {
					return ItemStack.EMPTY;
				}
			}
	
			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
	
			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}
	
			slot.onTake(player, itemstack1);
		}
	
		return itemstack;
	}
}