package spyeedy.testmod.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiCheckBox;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.capability.types.CapabilityTypes;
import spyeedy.testmod.capability.types.ITypes;

import java.awt.*;
import java.io.IOException;

public class GuiSelectAlienIcons extends GuiScreen {

	private final EntityPlayer player;

	private ITypes cap;
	
	public GuiSelectAlienIcons(EntityPlayer player) {
		this.player = player;
		this.cap = player.getCapability(CapabilityTypes.TYPES_CAP, null);
	}
	
	// ----------------------------------------------------------------------------------------------------------------------
	
	private static final ResourceLocation GUI = new ResourceLocation(TestMod.MODID, "textures/gui/colors_customizer.png");

	private int xSize = 176;
	private int ySize = 205;
	
	private int selectedAlienIcon = 0;
	private GuiCheckBox selectModeCheckbox;
	
	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;
		
		String alien1 = "Alien 1";
		GuiButton btnAlien1 = new GuiButtonExt(0, i + 6, j + 18, mc.fontRenderer.getStringWidth(alien1) + 10, 20, alien1);
		String alien2 = "Alien 2";
		GuiButton btnAlien2 = new GuiButtonExt(1, i + 10 + btnAlien1.getButtonWidth(), j + 18, mc.fontRenderer.getStringWidth(alien2) + 10, 20, alien2);
		
		selectModeCheckbox = new GuiCheckBox(2, i + 6, j + 44, "Do you want to select alien?", cap.isSelectingAlien());
		System.out.println("Selection Alien Mode:" + cap.isSelectingAlien());
		
		addButton(btnAlien1);
		addButton(btnAlien2);
		addButton(selectModeCheckbox);
		
		updateButtons(cap.getAlienIcon());
	}
	
	private void updateButtons(int alienIcon) {
		for (GuiButton btn : buttonList) {
			if (btn.id == alienIcon)
				btn.enabled = false;
			else
				btn.enabled = true;
		}
		selectedAlienIcon = alienIcon;
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		if (button.id <= 1) {
			updateButtons(button.id);
			cap.setAlienIcon(button.id);
		} else if (button == selectModeCheckbox) {
			selectModeCheckbox.setIsChecked(!selectModeCheckbox.isChecked());
			cap.setSelectAlienMode(!selectModeCheckbox.isChecked());
			System.out.println("Selection Alien Mode:" + cap.isSelectingAlien());
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.selectModeCheckbox.mousePressed(Minecraft.getMinecraft(), mouseX, mouseY);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.color(1F, 1F, 1F, 1F);
		mc.getTextureManager().bindTexture(GUI);
		this.drawTexturedModalRect(i, j, 0, 0, xSize, ySize);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
		
		String title = "TestMod's Omnitrix";
		fontRenderer.drawString(title, i + xSize/2 - fontRenderer.getStringWidth(title)/2, j + 4, Color.WHITE.getRGB(), true);
		
		String sub = "Selected Alien Icon: ";
		fontRenderer.drawString(sub, i + 6, j + 85, Color.DARK_GRAY.getRGB());
		fontRenderer.drawString(Integer.toString(cap.getAlienIcon()), i + 6 + fontRenderer.getStringWidth(sub), j + 85, Color.RED.getRGB());
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
}