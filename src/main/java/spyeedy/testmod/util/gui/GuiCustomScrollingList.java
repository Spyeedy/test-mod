package spyeedy.testmod.util.gui;

import java.awt.Color;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.MathHelper;

/**
 * 
 * Call the follow methods in your parent Gui: mouseClicked, handleMouseInput
 */
public abstract class GuiCustomScrollingList {
	protected int selectedIndex = -1;
	
	protected int scrollY = 0;
	protected int scrollLength;
	protected Minecraft mc;
	
	protected int listWidth, listHeight, slotHeight, slotWidth;
	protected int top, left, bottom, right;
	protected int screenWidth, screenHeight;
	
	private long lastClickTime = 0L;
	
	public GuiCustomScrollingList(Minecraft client, int width, int height, int top, int left, int slotHeight, int screenWidth, int screenHeight) {
		this.mc = client;
		
		this.listWidth = width;
		this.listHeight = height;
		
		this.top = top;
		this.left = left;
		this.bottom = this.top + this.listHeight;
		this.right = this.left + this.listWidth;
		
		this.slotHeight = slotHeight;
		this.slotWidth = this.listWidth - 10;
				
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}
	
	protected abstract int getListSize();
	
	protected abstract boolean isSelected(int index);
	
	protected abstract void slotClicked(int slotIdx, boolean doubleClick);

	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int actualMaxScrollLength = this.getListSize() * this.slotHeight;
		
		int hiddenListLength = actualMaxScrollLength - this.listHeight;
		this.scrollLength = this.listHeight - hiddenListLength;
		
		this.drawBackground();

		int viewHeight = bottom - this.top;
		
		ScaledResolution res = new ScaledResolution(mc);
		double scaleW = mc.displayWidth / res.getScaledWidth_double();
		double scaleH = mc.displayHeight / res.getScaledHeight_double();
		
		GlStateManager.pushMatrix();
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		GL11.glScissor((int)(this.left			* scaleW), (int)(mc.displayHeight - (this.bottom * scaleH)),
					   (int)(this.listWidth 	* scaleW), (int)(viewHeight * scaleH));
		for (int i = 0; i < this.getListSize(); i++) {
			int y = (this.top + i * this.slotHeight) - this.scrollY;

			// Draw Selection
			if (this.isSelected(i)) {
				GlStateManager.pushMatrix();
				Gui.drawRect(this.left, y, this.left + this.slotWidth, y + this.slotHeight, Color.RED.getRGB());
				Gui.drawRect(this.left + 1, y + 1, this.left + this.slotWidth - 1, y + this.slotHeight - 1, Color.BLACK.getRGB());
				GlStateManager.popMatrix();
			}
			
			GlStateManager.pushMatrix();
			GlStateManager.color(1, 1, 1);
			this.drawSlot(i, y, this.left, this.left + this.slotWidth, partialTicks);
			GlStateManager.color(1, 1, 1);
			GlStateManager.popMatrix();
		}
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
		GlStateManager.popMatrix();

		int scrollBarWidth = 10;
		// Scrollbar Background
		Gui.drawRect(this.right - scrollBarWidth, this.top, this.right, this.bottom, Color.BLACK.getRGB());
		
		if (this.listHeight < this.getListSize()*this.slotHeight) {
	
			// Scrollbar Shadow
			Gui.drawRect(this.right - scrollBarWidth, this.top + scrollY, this.right, this.top + scrollY + scrollLength, Color.DARK_GRAY.getRGB());
			
			// Scrollbar Main
			Gui.drawRect(this.right - scrollBarWidth, this.top + scrollY, this.right - 1, this.top + scrollY + scrollLength - 1, Color.LIGHT_GRAY.getRGB());
			
			/*handleMouseInput(mouseX, mouseY);
			mouseClickMove(mouseX, mouseY, Mouse.getEventButton());*/
		}
	}
	
	protected void drawBackground() {}
	
	protected abstract void drawSlot(int slotIdx, int slotTop, int slotLeft, int slotRight, float partialTicks);
	
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
		boolean isHovering = mouseX >= this.left && mouseX <= this.right && mouseY >= this.top && mouseY <= this.top + this.listHeight;
		
		if (mouseButton == 0 && isHovering) {
			for (int slotIndex = 0; slotIndex < this.getListSize(); slotIndex++) {
				
				if (mouseX >= this.left && mouseX <= this.left + this.slotWidth && mouseY >= this.top - this.scrollY + (slotIndex * this.slotHeight) && mouseY <= this.top - this.scrollY + this.slotHeight + (slotIndex * this.slotHeight)) {
					this.slotClicked(slotIndex, slotIndex == this.selectedIndex && System.currentTimeMillis() - this.lastClickTime < 250L);
					this.selectedIndex = slotIndex;
					this.lastClickTime = System.currentTimeMillis();
				}
			}
		}
	}
	
	public void handleMouseInput(int mouseX, int mouseY) {
		boolean isHovering = mouseX >= this.left && mouseX <= this.right && mouseY >= this.top && mouseY <= this.top + this.listHeight;
		if (isHovering) {
			int scroll = Mouse.getEventDWheel();
			if (scroll != 0) {
				int gap = (int)((-1 * scroll / 120.0F) * this.slotHeight / 2);
				int newScrollY = this.scrollY + gap;
				this.scrollY = MathHelper.clamp(newScrollY, 0, this.listHeight - scrollLength);
			}
		}
	}
}