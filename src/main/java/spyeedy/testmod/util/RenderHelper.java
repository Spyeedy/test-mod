package spyeedy.testmod.util;

import org.lwjgl.opengl.GL11;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class RenderHelper {
	
	public static Minecraft mc = Minecraft.getMinecraft();
	
	public static void drawRectWithTexture(double x, double y, float u, float v, int width, int height, float textureWidth, float textureHeight)
    {
		drawRectWithTexture(x, y, 0, u, v, width, height, textureWidth, textureHeight);
    }
	
	/**
	 * Texture size must be 256x256
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param u
	 * @param v
	 * @param width
	 * @param height
	 * @param textureWidth
	 * @param textureHeight
	 */
	public static void drawRectWithTexture(double x, double y, double z, float u, float v, int width, int height, float textureWidth, float textureHeight)
    {
		float scale = 0.00390625F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        buffer.pos((double)x, (double)(y + height), z).tex((double)(u * scale), (double)(v + textureHeight) * scale).endVertex();
        buffer.pos((double)(x + width), (double)(y + height), z).tex((double)(u + textureWidth) * scale, (double)(v + textureHeight) * scale).endVertex();
        buffer.pos((double)(x + width), (double)y, z).tex((double)(u + textureWidth) * scale, (double)(v * scale)).endVertex();
        buffer.pos((double)x, (double)y, z).tex((double)(u * scale), (double)(v * scale)).endVertex();
        tessellator.draw();
    }
	
	public static void drawRect(int left, int top, int right, int bottom, double red, double green, double blue, double alpha) {
		if (left < right) {
			int i = left;
			left = right;
			right = i;
		}

		if (top < bottom) {
			int j = top;
			top = bottom;
			bottom = j;
		}

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GL11.glColor3d(red, green, blue);
		worldrenderer.begin(7, DefaultVertexFormats.POSITION);
		worldrenderer.pos((double) left, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) top, 0.0D).endVertex();
		worldrenderer.pos((double) left, (double) top, 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}
	
	public static void drawRect(int left, int top, int right, int bottom, float red, float green, float blue, float alpha) {
		if (left < right) {
			int i = left;
			left = right;
			right = i;
		}

		if (top < bottom) {
			int j = top;
			top = bottom;
			bottom = j;
		}

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.color(red, green, blue);
		GlStateManager.color(red, green, blue, alpha);
		worldrenderer.begin(7, DefaultVertexFormats.POSITION);
		worldrenderer.pos((double) left, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) bottom, 0.0D).endVertex();
		worldrenderer.pos((double) right, (double) top, 0.0D).endVertex();
		worldrenderer.pos((double) left, (double) top, 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
	
	public static void drawStringWithOutline(String string, int posX, int posY, int fontColor, int outlineColor) {
		mc.fontRenderer.drawString(string, posX + 1, posY, outlineColor);
		mc.fontRenderer.drawString(string, posX - 1, posY, outlineColor);
		mc.fontRenderer.drawString(string, posX, posY + 1, outlineColor);
		mc.fontRenderer.drawString(string, posX, posY - 1, outlineColor);

		mc.fontRenderer.drawString(string, posX, posY, fontColor);
	}

	public static float median(double currentPos, double prevPos) {
		return (float) (prevPos + (currentPos - prevPos) * LCRenderHelper.renderTick);
	}

	public static void translateRendering(EntityPlayer player, Entity entity) {
		double x = -median(entity.posX, entity.prevPosX) - (median(player.posX, player.prevPosX) - median(entity.posX, entity.prevPosX));
		double y = -median(entity.posY, entity.prevPosY) - (median(player.posY, player.prevPosY) - median(entity.posY, entity.prevPosY));
		double z = -median(entity.posZ, entity.prevPosZ) - (median(player.posZ, player.prevPosZ) - median(entity.posZ, entity.prevPosZ));
		GL11.glTranslatef((float) x, (float) y, (float) z);
	}
	
	public static Vec3d rotateZ(Vec3d vector, double angle) {
		float x1 = (float) (vector.x * Math.cos(angle) - vector.y * Math.sin(angle));
		float y1 = (float) (vector.x * Math.sin(angle) + vector.y * Math.cos(angle));
		return new Vec3d(x1, y1, vector.z);
	}
}