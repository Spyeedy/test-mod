package spyeedy.testmod.util;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import spyeedy.testmod.TestMod;

public class ModelHelper {

	public static void registerItem(Item item, int meta, String name) {
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(new ResourceLocation(TestMod.MODID, name), "inventory"));
	}
	
	public static void registerItem(Item item, int meta) {
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}
	
	public static void registerItem(Item item) {
		registerItem(item, 0);
	}
	
	public static void registerBlock(Block block, int meta, String name) {
		registerItem(Item.getItemFromBlock(block), meta, name);
	}
	
	public static void registerBlock(Block block, int meta) {
		registerItem(Item.getItemFromBlock(block), meta);
	}
	
	public static void registerBlock(Block block) {
		registerBlock(block, 0);
	}
	
	public static void registerBlock(Block block, String name, String variant) {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(TestMod.MODID + ":" + name, variant));
	}
}