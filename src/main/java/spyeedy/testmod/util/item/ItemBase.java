package spyeedy.testmod.util.item;

import spyeedy.testmod.TestMod;

public class ItemBase extends lucraft.mods.lucraftcore.util.items.ItemBase {

	public ItemBase(String name) {
		super(name);
		this.setCreativeTab(TestMod.tabTestMod);
	}
}