package spyeedy.testmod.util;

public class Colors3D {

	public double red;
	public double green;
	public double blue;
	
	public Colors3D(double red, double green, double blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
}