package spyeedy.testmod.proxy;

import lucraft.mods.lucraftcore.extendedinventory.render.ExtendedInventoryItemRendererRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import spyeedy.testmod.capability.types.CapabilityTypes;
import spyeedy.testmod.capability.types.ITypes;
import spyeedy.testmod.capability.types.OmnitrixColorTypes;
import spyeedy.testmod.client.render.ItemRendererSomeRing;
import spyeedy.testmod.entity.TestEntity;
import spyeedy.testmod.events.ClientEventHandler;
import spyeedy.testmod.items.TMItems;

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		
		TestEntity.loadRenderers();
	}

	@Override
	public void init(FMLInitializationEvent event) {
		super.init(event);
		MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
		
		Minecraft mc = Minecraft.getMinecraft();
		
		IItemColor color = (stack, index) -> {
			ITypes cap = mc.player.getCapability(CapabilityTypes.TYPES_CAP, null);
			if (index == 0) {
				return cap.getColor(OmnitrixColorTypes.FACE);
			}
			if (index == 1) {
				return cap.getColor(OmnitrixColorTypes.WATCH_STRAP);
			}
			if (index == 2) {
				return cap.getColor(OmnitrixColorTypes.BODY);
			}
			if (index == 3) {
				return cap.getColor(OmnitrixColorTypes.RING);
			}
			return -1;
		};
		
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler(color, TMItems.OG_OMNITRIX);

		ExtendedInventoryItemRendererRegistry.registerRenderer(TMItems.SOME_RING, new ItemRendererSomeRing());
	}
}