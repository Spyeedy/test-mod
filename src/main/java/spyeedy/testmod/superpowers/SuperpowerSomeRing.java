package spyeedy.testmod.superpowers;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.EntityLivingBase;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.ability.AbilitySuitUp;

public class SuperpowerSomeRing extends Superpower {

	public SuperpowerSomeRing() {
		super("some_ring");
		this.setRegistryName(TestMod.MODID, "some_ring");
	}

	@Override
	public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context) {
		abilities.put("suit_up", new AbilitySuitUp(entity));
		return abilities;
	}
}
