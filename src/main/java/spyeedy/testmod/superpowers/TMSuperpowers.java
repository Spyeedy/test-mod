package spyeedy.testmod.superpowers;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.TestMod;

@Mod.EventBusSubscriber(modid = TestMod.MODID)
public class TMSuperpowers {

	public static Superpower SOME_RING;

	@SubscribeEvent
	public static void registerSuperpower(RegistryEvent.Register<Superpower> e) {
		e.getRegistry().register(SOME_RING = new SuperpowerSomeRing());
	}
}