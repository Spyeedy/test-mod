package spyeedy.testmod.client.render;

import lucraft.mods.lucraftcore.extendedinventory.render.ExtendedInventoryItemRendererRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;

public class ItemRendererSomeRing implements ExtendedInventoryItemRendererRegistry.IItemExtendedInventoryRenderer {

	@Override
	public void render(EntityPlayer player, RenderLivingBase<?> render, ItemStack stack, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale, boolean isHead) {
		if (isHead)
			return;


		boolean flag = player.getPrimaryHand() == EnumHandSide.RIGHT;

		if (!stack.isEmpty()) {
			GlStateManager.pushMatrix();

			if (render.getMainModel().isChild) {
				GlStateManager.translate(0.0F, 0.625F, 0.0F);
				GlStateManager.rotate(-20.0F, -1.0F, 0.0F, 0.0F);
				GlStateManager.scale(0.5F, 0.5F, 0.5F);
			}

			this.renderHeldItem(render, player, stack, flag ? ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND : ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND, flag ? EnumHandSide.RIGHT : EnumHandSide.LEFT);

			GlStateManager.popMatrix();
		}
	}

	private void renderHeldItem(RenderLivingBase<?> render, EntityLivingBase entity, ItemStack stack, ItemCameraTransforms.TransformType type, EnumHandSide handSide) {
		if (stack != null) {
			GlStateManager.pushMatrix();

			if (entity.isSneaking()) {
				GlStateManager.translate(0.0F, 0.2F, 0.0F);
			}

			((ModelBiped) render.getMainModel()).postRenderArm(0.0625F, handSide);
			GlStateManager.rotate(-90.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
			boolean flag = handSide == EnumHandSide.LEFT;
			GlStateManager.translate((flag ? -1 : 1) / 16.0F,  2 * 0.0625F, -10 * 0.0625F);
			Minecraft.getMinecraft().getItemRenderer().renderItemSide(entity, stack, type, flag);
			GlStateManager.popMatrix();
		}
	}
}