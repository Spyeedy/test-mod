package spyeedy.testmod;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import org.apache.logging.log4j.Logger;
import spyeedy.testmod.addonpack.model.modeltype.RegisterModelTypeEvent;
import spyeedy.testmod.capability.extraslot.CapabilityExtraSlot;
import spyeedy.testmod.capability.itemdata.CapabilityItemData;
import spyeedy.testmod.capability.types.ITypes;
import spyeedy.testmod.capability.types.TDefaultImplementation;
import spyeedy.testmod.capability.types.TypesStorage;
import spyeedy.testmod.creativetabs.CreativeTabTestMod;
import spyeedy.testmod.events.CommonEventHandler;
import spyeedy.testmod.gui.GuiHandler;
import spyeedy.testmod.network.PacketDispatcher;
import spyeedy.testmod.proxy.CommonProxy;

@Mod(modid = TestMod.MODID, version = TestMod.VERSION, name = TestMod.NAME)
public class TestMod {

	public static final String VERSION = "1.12-1.0";
	public static final String NAME = "Test Mod";
	public static final String MODID = "spytm";
	
	public static final int STORAGE_ID = 1;
	public static final int INVENTORY_ITEM_ID = 3;
	public static final int EXTRA_SLOTS_ID = 4;
	
	@SidedProxy(clientSide = "spyeedy.testmod.proxy.ClientProxy", serverSide = "spyeedy.testmod.proxy.CommonProxy", modId = MODID)
	public static CommonProxy proxy;
	
	@Mod.Instance
	public static TestMod instance;
	
	public static SimpleNetworkWrapper networkWrapper = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
	
	public static CreativeTabs tabTestMod = new CreativeTabTestMod();

	public static Logger LOGGER;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new RegisterModelTypeEvent());
		LOGGER = event.getModLog();
		
		proxy.preInit(event);
		CapabilityManager.INSTANCE.register(ITypes.class, new TypesStorage(), TDefaultImplementation::new);
		CapabilityExtraSlot.register();
		CapabilityItemData.register();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
		NetworkRegistry.INSTANCE.registerGuiHandler(TestMod.instance, new GuiHandler());
		MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
		PacketDispatcher.registerPackets();
	}
}