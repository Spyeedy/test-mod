package spyeedy.testmod.items.json;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.gui.GuiModels;

public class ItemModels extends Item {
	
	public ItemModels() {
		setTranslationKey("model_item");
		setRegistryName("model_item");
		setCreativeTab(TestMod.tabTestMod);
	}
	
	@Override	
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		if (handIn == EnumHand.MAIN_HAND) {
			if (worldIn.isRemote) {
				Minecraft.getMinecraft().displayGuiScreen(new GuiModels(playerIn));
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	};
}