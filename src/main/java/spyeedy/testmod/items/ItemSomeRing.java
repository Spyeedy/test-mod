package spyeedy.testmod.items;

import lucraft.mods.lucraftcore.extendedinventory.IItemExtendedInventory;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.superpowers.TMSuperpowers;

public class ItemSomeRing extends Item implements IItemExtendedInventory {

	public ItemSomeRing() {
		this.setTranslationKey("some_ring");
		this.setRegistryName("some_ring");

		this.setCreativeTab(TestMod.tabTestMod);
	}

	@Override
	public ExtendedInventoryItemType getEIItemType(ItemStack stack) {
		return ExtendedInventoryItemType.WRIST;
	}

	@Override
	public void onEquipped(ItemStack itemstack, EntityPlayer player) {
		SuperpowerHandler.giveSuperpower(player, TMSuperpowers.SOME_RING);
	}

	@Override
	public void onUnequipped(ItemStack itemstack, EntityPlayer player) {
		SuperpowerHandler.removeSuperpower(player);
	}
}