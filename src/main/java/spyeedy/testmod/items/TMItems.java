package spyeedy.testmod.items;

import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.items.json.ItemModels;
import spyeedy.testmod.util.ModelHelper;

@EventBusSubscriber(modid = TestMod.MODID)
public class TMItems {

	public static Item MODELS = new ItemModels();
	public static Item INVENTORY_ITEM = new ItemInventory();
	public static Item DATA_ITEM = new ItemData();
	public static Item OG_OMNITRIX = new ItemOGOmnitrix();
	public static ItemSomeRing SOME_RING = new ItemSomeRing();
			
	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(
				MODELS,
				INVENTORY_ITEM,
				DATA_ITEM,
				OG_OMNITRIX,
				SOME_RING
		);
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onRegisterModels(ModelRegistryEvent event) {
		ModelHelper.registerItem(MODELS);
		ModelHelper.registerItem(INVENTORY_ITEM);
		ModelHelper.registerItem(DATA_ITEM);
		ModelHelper.registerItem(OG_OMNITRIX);
		ModelHelper.registerItem(SOME_RING);
	}
}