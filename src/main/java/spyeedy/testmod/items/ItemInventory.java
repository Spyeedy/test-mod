package spyeedy.testmod.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import spyeedy.testmod.TestMod;

public class ItemInventory extends Item {
	
	public ItemInventory() {
		this.setTranslationKey("inventory_item");
		this.setRegistryName("inventory_item");
		this.setCreativeTab(TestMod.tabTestMod);
		
		this.setMaxStackSize(1);
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt) {
		return new InventoryCapabilityProvider();
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		if(!playerIn.isSneaking() && handIn == EnumHand.MAIN_HAND) {
			playerIn.openGui(TestMod.instance, TestMod.INVENTORY_ITEM_ID, worldIn, 0, 0, 0);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	private class InventoryCapabilityProvider implements ICapabilitySerializable<NBTTagCompound> {

		private ItemStackHandler inventory = new ItemStackHandler(9);
		
		@Override
		public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
			return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
		}
		
		@Override
		public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
			return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T) inventory : null;
		}

		@Override
		public NBTTagCompound serializeNBT() {
			NBTTagCompound compound = new NBTTagCompound();
			compound.setTag("inventory", inventory.serializeNBT());
			return compound;
		}

		@Override
		public void deserializeNBT(NBTTagCompound nbt) {
			inventory.deserializeNBT(nbt.getCompoundTag("inventory"));
			System.out.println("Reading");
		}
	}
}