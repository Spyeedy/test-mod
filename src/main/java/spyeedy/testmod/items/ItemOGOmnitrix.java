package spyeedy.testmod.items;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.capability.types.CapabilityTypes;
import spyeedy.testmod.capability.types.ITypes;
import spyeedy.testmod.gui.GuiOmnitrixColors;
import spyeedy.testmod.gui.GuiSelectAlienIcons;
import spyeedy.testmod.util.item.ItemBase;

public class ItemOGOmnitrix extends ItemBase {

	public ItemOGOmnitrix() {
		super("og_omnitrix");
		this.maxStackSize = 1;
		this.addPropertyOverride(new ResourceLocation(TestMod.MODID, "alien_icon"), (stack, world, entity) -> {
			if (entity instanceof EntityPlayer) {
				ITypes cap = entity.getCapability(CapabilityTypes.TYPES_CAP, null);
				
				if (!stack.isEmpty() && stack.getItem() == this) {
					if (cap.isSelectingAlien() == false) {
						return 0f;
					} else {
						float[] array = { 0.5f, 0.75f };
						return array[cap.getAlienIcon()];
					}
				}
			}
			return 0f;
		});
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		if (playerIn.isSneaking()) {
			if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
				Minecraft.getMinecraft().displayGuiScreen(new GuiOmnitrixColors(playerIn));
			}
		} else {
			if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
				Minecraft.getMinecraft().displayGuiScreen(new GuiSelectAlienIcons(playerIn));
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}