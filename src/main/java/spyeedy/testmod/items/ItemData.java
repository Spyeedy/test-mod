package spyeedy.testmod.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import spyeedy.testmod.capability.itemdata.CapabilityItemData;
import spyeedy.testmod.capability.itemdata.IItemData;
import spyeedy.testmod.capability.itemdata.ItemDataImpl;
import spyeedy.testmod.util.item.ItemBase;

public class ItemData extends ItemBase {

	public ItemData() {
		super("data_item");
		this.maxStackSize = 1;
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt) {
		return new ICapabilityProvider() {

			private IItemData cap = new ItemDataImpl(stack, "coins", 0);
			
			@Override
			public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
				return capability == CapabilityItemData.ITEM_DATA_CAPABILITY;
			}

			@Override
			public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
				return capability == CapabilityItemData.ITEM_DATA_CAPABILITY ? (T) cap : null;
			}
		};
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack stack = playerIn.getHeldItem(handIn);
		IItemData cap = stack.getCapability(CapabilityItemData.ITEM_DATA_CAPABILITY, null);
		if (!playerIn.isSneaking()) {
			int coins = cap.getCoins();
			if (!worldIn.isRemote) playerIn.sendMessage(new TextComponentString("Coins:" + coins));
		} else {
			cap.adjustCoins(10, true);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}