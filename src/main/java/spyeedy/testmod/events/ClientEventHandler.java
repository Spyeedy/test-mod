package spyeedy.testmod.events;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.network.PacketDispatcher;
import spyeedy.testmod.network.server.MessageOpenExtraSlots;

public class ClientEventHandler {

	private Minecraft mc = Minecraft.getMinecraft();

	@SubscribeEvent
	public void onGuiOpen(GuiScreenEvent.InitGuiEvent.Post e) {
		if (e.getGui() instanceof GuiContainerCreative) {
			int k = (e.getGui().width - 195) / 2;
			int l = (e.getGui().height - 136) / 2;

			String name = "Extra Slot";
			e.getButtonList().add(new GuiButtonExt(80, k + 200, l + 140, mc.fontRenderer.getStringWidth(name) + 10, 20, name));
		}
	}

	@SubscribeEvent
	public void onButtonPressPre(GuiScreenEvent.ActionPerformedEvent.Post e) {
		if (e.getGui() instanceof GuiContainerCreative && e.getButton().id == 80) {
			PacketDispatcher.sendToServer(new MessageOpenExtraSlots());
		}
	}
}