package spyeedy.testmod.capability.types;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import spyeedy.testmod.TestMod;
import spyeedy.testmod.network.client.MessageUpdateTypes;
import spyeedy.testmod.network.server.MessageSyncUpdateTypes;

public class TDefaultImplementation implements ICapabilitySerializable<NBTTagCompound>, ITypes {

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return CapabilityTypes.TYPES_CAP != null && capability == CapabilityTypes.TYPES_CAP;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if (CapabilityTypes.TYPES_CAP != null && capability == CapabilityTypes.TYPES_CAP)
			return CapabilityTypes.TYPES_CAP.cast(this);
		return null;
	}
	
	public void sync(EntityPlayer player) {
		if (!player.world.isRemote)
			TestMod.networkWrapper.sendTo(new MessageUpdateTypes(this), (EntityPlayerMP) player);
	}
	
	public void sync() {
		TestMod.networkWrapper.sendToServer(new MessageSyncUpdateTypes(this));
	}
	
	// ------------------------------------------------------------------------------------------------------
	
	private int faceColor = 39183;
	private int watchStrapColor = 15658984;
	private int bodyColor = 1644825;
	private int ringColor = 1644825;
	
	private boolean isSelectingAlien = false;
	private int alienIcon = 0;
	
	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = new NBTTagCompound();
		
		nbt.setInteger("faceColor", faceColor);
		nbt.setInteger("watchStrapColor", watchStrapColor);
		nbt.setInteger("bodyColor", bodyColor);
		nbt.setInteger("ringColor", ringColor);
		
		nbt.setBoolean("isSelectingAlien", isSelectingAlien);
		nbt.setInteger("alienIcon", alienIcon);
		return nbt;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {		
		this.faceColor = nbt.getInteger("faceColor");
		this.watchStrapColor = nbt.getInteger("watchStrapColor");
		this.bodyColor = nbt.getInteger("bodyColor");
		this.ringColor = nbt.getInteger("ringColor");

		this.isSelectingAlien = nbt.getBoolean("isSelectingAlien");
		this.alienIcon = nbt.getInteger("alienIcon");
	}
	
	// ------------------------------------------------------------------------------------------------------

	@Override
	public int getColor(OmnitrixColorTypes type) {
		switch(type) {
		case BODY:
			return bodyColor;
		case FACE:
			return faceColor;
		case RING:
			return ringColor;
		case WATCH_STRAP:
			return watchStrapColor;
		default:
			return bodyColor;
		}
	}

	@Override
	public void setColor(OmnitrixColorTypes type, String hex) {
		switch(type) {
		case BODY:
			this.bodyColor = hexToDecimal(hex);
			break;
		case FACE:
			this.faceColor = hexToDecimal(hex);
			break;
		case RING:
			this.ringColor = hexToDecimal(hex);
			break;
		case WATCH_STRAP:
			this.watchStrapColor = hexToDecimal(hex);
			break;
		}
		this.sync();
	}
	
	private int hexToDecimal(String hex) {
		String hexTable = "0123456789ABCDEF";
		hex = hex.toUpperCase();
		int val = 0;
		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(i);
			int hexVal = hexTable.indexOf(c);
			
			int power = hex.length() - 1 - i;
			val += hexVal * Math.pow(16, power);
		}
		return val;
	}

	@Override
	public void resetColors(OmnitrixColorTypes type) {
		switch(type) {
		case BODY:
			setColor(type, "191919");
			break;
		case FACE:
			setColor(type, "00990F");
			break;
		case RING:
			setColor(type, "191919");
			break;
		case WATCH_STRAP:
			setColor(type, "EEEFE8");
			break;
		}
	}

	@Override
	public int getAlienIcon() {
		return alienIcon;
	}

	@Override
	public void setAlienIcon(int _new) {
		this.alienIcon = _new;
		this.sync();
	}

	@Override
	public boolean isSelectingAlien() {
		return isSelectingAlien;
	}

	@Override
	public void setSelectAlienMode(boolean _new) {
		this.isSelectingAlien = _new;
		this.sync();
	}
}