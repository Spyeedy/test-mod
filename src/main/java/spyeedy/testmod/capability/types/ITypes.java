package spyeedy.testmod.capability.types;

import net.minecraft.util.math.Vec3d;

public interface ITypes {
	
	int getColor(OmnitrixColorTypes type);
	void setColor(OmnitrixColorTypes type, String hex);
	void resetColors(OmnitrixColorTypes type);
	
	boolean isSelectingAlien();
	void setSelectAlienMode(boolean _new);
	
	int getAlienIcon();
	void setAlienIcon(int _new);
}