package spyeedy.testmod.capability.types;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.TestMod;

@EventBusSubscriber
public class CapabilityTypes {
	
	@CapabilityInject(ITypes.class)
	public static Capability<ITypes> TYPES_CAP = null;

	@SubscribeEvent
	public static void attachCapabilities(AttachCapabilitiesEvent<Entity> e) {
		if (e.getObject() instanceof EntityPlayer)
			e.addCapability(new ResourceLocation(TestMod.MODID, "typesData"), new TDefaultImplementation());
	}

	@SubscribeEvent
	public static void cloneCapabilitiesEvent(PlayerEvent.Clone event)
	{
		//Should only fire on return from death, otherwise the data is already there
		if (event.isWasDeath())
		{
			TDefaultImplementation writePlayerDataNBT = (TDefaultImplementation) event.getOriginal().getCapability(TYPES_CAP, null);
			NBTTagCompound selectedSlotNBT = writePlayerDataNBT.serializeNBT();
			TDefaultImplementation readPlayerDataNBT = (TDefaultImplementation) event.getEntityPlayer().getCapability(TYPES_CAP, null);
			readPlayerDataNBT.deserializeNBT(selectedSlotNBT);
		}
	}

	@SubscribeEvent
	public static void onPlayerLoginEvent(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof EntityPlayerMP && event.getEntity().hasCapability(TYPES_CAP, null)) {
			EntityPlayer player = (EntityPlayer) event.getEntity();
			TDefaultImplementation playerData = (TDefaultImplementation) event.getEntity().getCapability(TYPES_CAP, null);
			playerData.sync(player);
		}
	}
}