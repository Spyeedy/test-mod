package spyeedy.testmod.capability.types;

public enum OmnitrixColorTypes {

	FACE, WATCH_STRAP, BODY, RING;
	
	public String getName() {
		String s = "";

		for (int i = 0; i < this.toString().toCharArray().length; i++) {
			char c = this.toString().toCharArray()[i];
			
			int pos = 0;
			
			if (c == '_' && i > 0) {
				c = ' ';
				//s = s + " ";
				pos = i + 1;
			}
			
			if (i > 0) {
				c = Character.toLowerCase(c);
			}
				

			s = s + c;
		}
		
		if (s.contains(" ")) {
			int pos = s.indexOf(' ');
			
			int newPos = pos + 1;
			char c = s.charAt(newPos);
			s = s.substring(0, newPos) + Character.toUpperCase(c) + s.substring(newPos + 1);
		}

		return s;
	}
	
	public static OmnitrixColorTypes getTrailTypeFromName(String name) {
		for(OmnitrixColorTypes types : values()) {
			if(types.toString().equalsIgnoreCase(name)) {
				return types;
			}
		}
		return FACE;
	}
}