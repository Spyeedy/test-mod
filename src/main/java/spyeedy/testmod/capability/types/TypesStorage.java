package spyeedy.testmod.capability.types;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class TypesStorage implements IStorage<ITypes> {

	@Override
	public NBTBase writeNBT(Capability<ITypes> capability, ITypes instance, EnumFacing side) {
		if (instance instanceof TDefaultImplementation)
			((TDefaultImplementation) instance).serializeNBT();
		return null;
	}

	@Override
	public void readNBT(Capability<ITypes> capability, ITypes instance, EnumFacing side, NBTBase nbt) {
		if (instance instanceof TDefaultImplementation && nbt instanceof NBTTagCompound)
			((TDefaultImplementation) instance).deserializeNBT((NBTTagCompound) nbt);
	}
}