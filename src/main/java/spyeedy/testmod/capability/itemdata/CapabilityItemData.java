package spyeedy.testmod.capability.itemdata;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

public class CapabilityItemData {

	@CapabilityInject(IItemData.class)
	public static Capability<IItemData> ITEM_DATA_CAPABILITY = null;
	
	public static void register() {
		CapabilityManager.INSTANCE.register(IItemData.class, new IStorage<IItemData>() {
			@Override
			public NBTBase writeNBT(Capability<IItemData> capability, IItemData instance, EnumFacing side) {
				return null;
			}

			@Override
			public void readNBT(Capability<IItemData> capability, IItemData instance, EnumFacing side, NBTBase nbt) {}
		}, ItemDataImpl::new);
	}
}