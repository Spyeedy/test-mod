package spyeedy.testmod.capability.itemdata;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ItemDataImpl implements IItemData {
	
	private ItemStack stack;
	
	public ItemDataImpl() {
		this(ItemStack.EMPTY);
	}
	
	public ItemDataImpl(ItemStack stack) {
		this(stack, null, 0);
	}

	/**
	 * @param type - 'coins' or 'selected_model'
	 */
	public ItemDataImpl(ItemStack stack, String type, int number) {
		this.stack = stack;
		
		if (!stack.hasTagCompound()) {
			this.stack.setTagCompound(new NBTTagCompound());
			if (type.toLowerCase().equals("coins")) {
				this.setCoins(number);
			}
		}
	}

	@Override
	public void setCoins(int coin) {
		stack.getTagCompound().setInteger(NBTKeys.COINS, coin);
	}
	
	@Override
	public int getCoins() {
		return stack.getTagCompound().getInteger(NBTKeys.COINS);
	}

	@Override
	public void adjustCoins(int amount, boolean increase) {
		int coin = getCoins();
		this.setCoins(coin + (increase ? amount : -amount));
	}
	
	// ---------------------------------------
	
	public class NBTKeys {
		public static final String COINS = "Coins";
	}
}