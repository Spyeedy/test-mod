package spyeedy.testmod.capability.itemdata;

public interface IItemData {

	void setCoins(int coin);	
	int getCoins();
	
	void adjustCoins(int amount, boolean increase);
}