package spyeedy.testmod.capability.extraslot;

import lucraft.mods.lucraftcore.extendedinventory.InventoryExtendedInventory;
import lucraft.mods.lucraftcore.extendedinventory.capabilities.CapabilityExtendedInventory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import spyeedy.testmod.TestMod;

@Mod.EventBusSubscriber
public class CapabilityExtraSlot {

	@CapabilityInject(IExtraSlot.class)
	public static Capability<IExtraSlot> EXTRA_SLOT_CAPABILITY = null;
	
	public static void register() {
		CapabilityManager.INSTANCE.register(IExtraSlot.class, new ExtraSlotStorage(), ExtraSlotInventory::new);
	}
	
	@SubscribeEvent
	public static void attachCapabilities(AttachCapabilitiesEvent<Entity> e) {
		if (e.getObject() instanceof EntityPlayer)
			e.addCapability(new ResourceLocation(TestMod.MODID, "extraSlot"), new ExtraSlotInventory());
	}

	@SubscribeEvent
	public static void onClonePlayer(PlayerEvent.Clone event) {
		NBTTagCompound compound = new NBTTagCompound();
		compound = (NBTTagCompound) EXTRA_SLOT_CAPABILITY.getStorage().writeNBT(EXTRA_SLOT_CAPABILITY, event.getOriginal().getCapability(EXTRA_SLOT_CAPABILITY, null), null);
		EXTRA_SLOT_CAPABILITY.getStorage().readNBT(EXTRA_SLOT_CAPABILITY, event.getEntityPlayer().getCapability(EXTRA_SLOT_CAPABILITY, null), null, compound);
	}

	@SubscribeEvent
	public void onPlayerDrops(PlayerDropsEvent e) {
		if (!e.getEntityPlayer().world.isRemote && !e.getEntityPlayer().world.getGameRules().getBoolean("keepInventory")) {
			if (e.getEntityPlayer().hasCapability(EXTRA_SLOT_CAPABILITY, null)) {
				ExtraSlotInventory inv = (ExtraSlotInventory) e.getEntityPlayer().getCapability(EXTRA_SLOT_CAPABILITY, null);

				for (int i = 0; i < inv.getSlots(); i++) {
					if (!inv.getStackInSlot(i).isEmpty()) {
						EntityItem en = new EntityItem(e.getEntityPlayer().world, e.getEntityPlayer().posX, e.getEntityPlayer().posY + e.getEntityPlayer().eyeHeight, e.getEntityPlayer().posZ, inv.getStackInSlot(i).copy());
						en.setPickupDelay(40);
						float f1 = e.getEntityPlayer().world.rand.nextFloat() * 0.5F;
						float f2 = e.getEntityPlayer().world.rand.nextFloat() * (float) Math.PI * 2.0F;
						en.motionX = (double) (-MathHelper.sin(f2) * f1);
						en.motionZ = (double) (MathHelper.cos(f2) * f1);
						en.motionY = 0.20000000298023224D;
						e.getDrops().add(en);
						inv.setStackInSlot(i, ItemStack.EMPTY);
					}
				}
			}
		}
	}
}