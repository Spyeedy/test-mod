package spyeedy.testmod.capability.extraslot;

import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public interface IExtraSlot extends IItemHandler {}