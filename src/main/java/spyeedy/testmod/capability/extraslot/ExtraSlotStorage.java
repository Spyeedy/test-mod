package spyeedy.testmod.capability.extraslot;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class ExtraSlotStorage implements Capability.IStorage<IExtraSlot> {

	@Override
	public NBTBase writeNBT(Capability<IExtraSlot> capability, IExtraSlot instance, EnumFacing side) {
		return ((ExtraSlotInventory) instance).serializeNBT();
	}

	@Override
	public void readNBT(Capability<IExtraSlot> capability, IExtraSlot instance, EnumFacing side, NBTBase nbt) {
		((ExtraSlotInventory) instance).deserializeNBT((NBTTagCompound) nbt);
	}
}