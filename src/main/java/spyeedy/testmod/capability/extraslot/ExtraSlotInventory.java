package spyeedy.testmod.capability.extraslot;

import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.items.ItemStackHandler;

public class ExtraSlotInventory extends ItemStackHandler implements ICapabilityProvider, IExtraSlot {
	
	public ExtraSlotInventory() {
		super(1);
	}
	
	@Override
	public int getSlotLimit(int slot) {
		return 1;
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityExtraSlot.EXTRA_SLOT_CAPABILITY;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityExtraSlot.EXTRA_SLOT_CAPABILITY ? (T) this : null;
	}
}